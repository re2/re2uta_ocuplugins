/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#ifndef NNPLUGIN_H_
#define NNPLUGIN_H_


#include <ros/ros.h>

#include <rviz/panel.h>

#include <QWidget>
#include <QComboBox>
#include <QPushButton>

class ImgHandlerPlugin : public rviz::Panel
{
    typedef rviz::Panel Super;

    Q_OBJECT

public:
    ImgHandlerPlugin();

    virtual void saveToConfig(   const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config );
    virtual void loadFromConfig( const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config );
    virtual void onInitialize();

//public slots:
private slots:
    void handleButton();

private:
    ros::NodeHandle m_node;
    ros::Timer m_timer;
    ros::Publisher  m_requestPub;
    std::string     m_rosTopic;

    QComboBox   *m_cameraCombo;
    QComboBox   *m_commLvlCombo;
    QPushButton *m_requestButton;

    char m_handlerId;

    void update( const ros::TimerEvent& e);
};



#endif /* NNPLUGIN_H_ */
