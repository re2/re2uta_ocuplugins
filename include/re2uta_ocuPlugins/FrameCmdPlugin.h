/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#ifndef FRAME_CMD_PLUGIN_H_
#define FRAME_CMD_PLUGIN_H_


#include <ros/ros.h>

#include <rviz/panel.h>

#include <QWidget>
#include <QComboBox>
#include <QPushButton>
#include <QLabel>
#include <QDoubleSpinBox>
#include <re2uta_ocuPlugins/SliderAndSpinBox.h>

#include <re2uta_atlasCommander/FrameCommandStamped.h>

class FrameCmdPlugin : public rviz::Panel
{
    typedef rviz::Panel Super;

    Q_OBJECT

public:
    FrameCmdPlugin();

    virtual void saveToConfig(   const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config );
    virtual void loadFromConfig( const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config );
    virtual void onInitialize();

//public slots:
private slots:
    void sendButton();
    void resetButton();

private:
    ros::NodeHandle m_node;
    ros::Publisher  m_cmdPub;
    re2uta_atlasCommander::FrameCommandStamped m_msg;
    QComboBox   *m_combo[3];
    QLabel            *m_label[9];
    QDoubleSpinBox *m_term[6];
    QPushButton *m_sendButton;
    QPushButton *m_resetButton;
    SliderAndSpinBox *m_weightSlider[6];
};



#endif /* FRAME_CMD_PLUGIN_H_ */
