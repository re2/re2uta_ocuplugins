/**
 *
 * walkWidget: this will handle Qt stuff
 *
 * ...
 *
 * @author Isura Ranatunga, University of Texas at Arlington, Copyright (C) 2013.
 * @contact isura.ranatunga@mavs.uta.edu
 * @see https://drc.resquared.com/trac/browser/sandboxes/david_rusbarsky/re2uta_ocuPlugins/include/re2uta_ocuPlugins/NNPlugin.h
 * @created 03/16/2013
 * @modified 05/21/2013
 *
 */

#ifndef WALKWIDGET_H_
#define WALKWIDGET_H_


#include <ros/ros.h>

#include <rviz/panel.h>

#include <QWidget>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QPushButton>
#include <QRadioButton>
#include <QLabel>
//#include <re2uta_ocuPlugins/HandWidget.h>

class walkPlugin : public rviz::Panel
{
          typedef rviz::Panel Super;

          Q_OBJECT

  public:
          walkPlugin();

          virtual void saveToConfig(   const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config );
          virtual void loadFromConfig( const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config );
          virtual void onInitialize();

  //public slots:

  private:
          ros::NodeHandle m_node;
          ros::Timer m_timer;
          ros::Publisher  traj_pub_, occu_pub_, floo_pub_, pose_pub_,solv_pub_, debug_pub_, nn_pub_;
          std::string     m_rosTopic;

          QLabel *m_a1Label;
          QLabel *m_a2Label;
          QLabel *m_a3Label;
          QLabel *m_a4Label;
          QLabel *m_a5Label;
          QLabel *m_a6Label;
          QLabel *m_a7Label;
          QLabel *m_a8Label;
          QLabel *m_a9Label;
          QLabel *m_a10Label;

          QPushButton    *m_a1Term;
          QPushButton    *m_a2Term;
          QPushButton    *m_a3Term;
          QPushButton    *m_a4Term;
          QPushButton    *m_a5Term;
          QPushButton    *m_a6Term;
          QRadioButton   *m_a7Term;
          QRadioButton   *m_a8Term;
          QRadioButton   *m_a9Term;
          QPushButton    *m_a10Term;

          void update( const ros::TimerEvent& e);
};

#endif /* WALKWIDGET_H_ */
