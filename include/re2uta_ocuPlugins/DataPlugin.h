/*
 * DataPlugin.h
 *
 *  Created on: Apr 12, 2013
 *      Author: drc
 */

#ifndef DATAPLUGIN_H_
#define DATAPLUGIN_H_

#include <ros/ros.h>

#include <rviz/panel.h>

#include <std_msgs/Int64.h>
#include <std_msgs/Empty.h>
#include <atlas_msgs/VRCScore.h>

#include <QWidget>
#include <QProgressBar>
#include <QComboBox>
#include <QPushButton>

class DataPlugin : public rviz::Panel
{
    typedef rviz::Panel Super;

    Q_OBJECT

public:
    DataPlugin();

    virtual void saveToConfig(   const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config );
    virtual void loadFromConfig( const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config );
    virtual void onInitialize();

public slots:
    void setDataScenario();
    void updateButton();

private:
    ros::NodeHandle m_node;
    ros::Timer m_timer;
    ros::Publisher m_requestPub;
    ros::Subscriber m_dataUpSub;
    ros::Subscriber m_dataDownSub;
    ros::Subscriber m_scoreSub;

    QPushButton *m_update;
    QComboBox *m_scenario;
    QProgressBar *m_dataProgressUp;
    QProgressBar *m_dataProgressDown;
    QProgressBar *m_timeProgress;

    void update( const ros::TimerEvent& e);
    void dataUpCB( const std_msgs::Int64ConstPtr& msg );
    void dataDownCB( const std_msgs::Int64ConstPtr& msg );
    void timeCB( const atlas_msgs::VRCScore& msg );
};



#endif /* DATAPLUGIN_H_ */
