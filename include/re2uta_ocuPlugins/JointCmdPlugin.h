/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#ifndef JOINT_CMD_PLUGIN_H_
#define JOINT_CMD_PLUGIN_H_


#include <ros/ros.h>

// LEAVE THIS HERE
#include <re2uta/AtlasDebugPublisher.hpp>

#include <rviz/panel.h>


#include <sensor_msgs/JointState.h>
#include <re2uta_inetComms/JointCommand.h>
#include <re2uta_atlasCommander/FrameCommandStamped.h>
#include <visualization_msgs/InteractiveMarkerFeedback.h>
#include <std_msgs/Empty.h>
#include <re2uta_ocuPlugins/armik.h>
#include <urdf/model.h>

#include <tf/transform_listener.h>

#include <QWidget>
#include <QDoubleSpinBox>
#include <QPushButton>
#include <QLabel>
#include <re2uta_ocuPlugins/SliderAndSpinBox.h>


#define CMD_CHANGE (0.5*D2R)

namespace re2uta
{

class JointCmdPlugin : public rviz::Panel
{
    typedef rviz::Panel Super;

    Q_OBJECT

public:
    JointCmdPlugin();
    virtual ~JointCmdPlugin();

    virtual void saveToConfig(   const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config );
    virtual void loadFromConfig( const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config );
    virtual void onInitialize();


private slots:
    void sendJointCmd();
    void sendFrameCmd();
    void updateJointState();
    void resetJointStateActual();
    void resetJointStateIM();
//    void updateButtonCB();

private:
    ros::NodeHandle m_node;
    ros::Publisher m_reqJointStatePub;
    ros::Subscriber m_jointStateSub;
    
    ros::Publisher m_jointCmdPub;
    ros::Publisher m_frameCmdPub;

    ros::Subscriber m_imSub;
    ros::Subscriber m_markerSub;

    sensor_msgs::JointState m_currentAngles;
    sensor_msgs::JointState m_imJoints;

    re2uta_inetComms::JointCommand m_jointCmdMsg;

    visualization_msgs::InteractiveMarkerFeedback m_imMarkers;

    tf::TransformListener m_tfListener;

    QLabel *m_label[43];
    QDoubleSpinBox *m_term[56];
    QPushButton *m_button[9];
    SliderAndSpinBox *m_weightSlider[6];

    void jointStateCallback(const sensor_msgs::JointState::ConstPtr& msg);
    void imCallback(const sensor_msgs::JointState::ConstPtr& msg);
    void imMarkerCallback(const visualization_msgs::InteractiveMarkerFeedback::ConstPtr& msg);


    void handleGhostPublishTimer( const ros::TimerEvent & event );
    // ghost stuff
    boost::shared_ptr<re2uta::AtlasDebugPublisher> m_atlasDebugPublisher;
    ros::Timer  m_ghostPublishTimer;
//    ros::Timer  m_DEBUGTimer;
    urdf::Model m_urdfModel;
 //   void DEBUGTimer( const ros::TimerEvent & event );

};

} // end namespace

#endif /* JOINT_CMD_PLUGIN_H_ */
