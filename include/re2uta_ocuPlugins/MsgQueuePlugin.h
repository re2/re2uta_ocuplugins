/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#ifndef MSGQUEUEPLUGIN_H_
#define MSGQUEUEPLUGIN_H_

#include <ros/ros.h>

#include <rviz/panel.h>

#include <QWidget>
#include <QListWidget>
#include <QPushButton>

#include <re2uta_inetComms/QueueInfo.h>

#define REQ_MSG_QUEUE         0
#define EXECUTE_MSG_QUEUE     1
#define REMOVE_FROM_MSG_QUEUE 2
#define CLEAR_MSG_QUEUE       3
#define THIS_IS_MSG_QUEUE     4

class MsgQueuePlugin : public rviz::Panel
{
    typedef rviz::Panel Super;

    Q_OBJECT

public:
    MsgQueuePlugin();

    virtual void saveToConfig(   const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config );
    virtual void loadFromConfig( const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config );
    virtual void onInitialize();

private slots:
  void executeQueue();
  void removeItem();
  void removeAllItems();
  void requestQueue();

private:
    ros::NodeHandle m_node;
    ros::Publisher  m_queueActionPub; // Request the queue
    ros::Subscriber m_queueSub;   // Listen for the queue

    QListWidget *m_listWidget;
    QPushButton *m_runQueueButton;
    QPushButton *m_removeButton;
    QPushButton *m_removeAllButton;
    QPushButton *m_requestQueueButton;

    void updateQueue(const re2uta_inetComms::QueueInfoConstPtr& msg);
};

#endif /* MSGQUEUEPLUGIN_H_ */
