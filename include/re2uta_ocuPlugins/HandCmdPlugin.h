/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#ifndef HAND_CMD_PLUGIN_H_
#define HAND_CMD_PLUGIN_H_

#include <ros/ros.h>
#include <rviz/panel.h>

#include <sensor_msgs/JointState.h>
#include <re2uta_inetComms/JointCommand.h>
#include <std_msgs/Empty.h>

#include <QWidget>
#include <QDoubleSpinBox>
#include <QPushButton>
#include <QLabel>
#include <re2uta_ocuPlugins/SliderAndSpinBox.h>

#define D2R (0.017453292)
#define R2D (57.29577951)
#define CMD_CHANGE (0.5*D2R)


class HandCmdPlugin : public rviz::Panel
{
    typedef rviz::Panel Super;

    Q_OBJECT

public:
    HandCmdPlugin();
    virtual ~HandCmdPlugin();

    virtual void saveToConfig(   const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config );
    virtual void loadFromConfig( const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config );
    virtual void onInitialize();

private slots:
    void sendLeftCmd();
    void sendRightCmd();
    void updateLeftState();
    void updateRightState();
    void hose1();
    void hose2();
    void hose3();
    void hose4();

    void saveP3();
    void saveP4();
    void saveP5();
    void saveP6();
    void saveP7();
    void saveP8();
    void saveP9();
    void saveP10();
    void saveP11();

    void doP3();
    void doP4();
    void doP5();
    void doP6();
    void doP7();
    void doP8();
    void doP9();
    void doP10();
    void doP11();

private:
    ros::NodeHandle m_node;
    ros::Publisher m_reqLeftHandStatePub;
    ros::Subscriber m_leftHandStateSub;
    ros::Publisher m_leftHandCmdPub;

    ros::Publisher m_reqRightHandStatePub;
    ros::Subscriber m_rightHandStateSub;
    ros::Publisher m_rightHandCmdPub;
    
    sensor_msgs::JointState m_currentLeftHand;
    sensor_msgs::JointState m_currentRightHand;

    re2uta_inetComms::JointCommand m_leftHandCmdMsg;
    re2uta_inetComms::JointCommand m_rightHandCmdMsg;
    
    QLabel *m_label[8];
    QDoubleSpinBox *m_term[24];
    QPushButton *m_button[26];
    SliderAndSpinBox *m_sliderSpin[24];

    void leftHandStateCallback(const sensor_msgs::JointState::ConstPtr& msg);
    void rightHandStateCallback(const sensor_msgs::JointState::ConstPtr& msg);

    bool m_presetValid[11];
    sensor_msgs::JointState m_presets[2][11];
};

#endif /* HAND_CMD_PLUGIN_H_ */
