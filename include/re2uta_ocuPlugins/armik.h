

#define LEFT  (0)
#define RIGHT (1)

#define PALM_OFFSET (0.16)
// joint offsets
#define U_TORSO_Z (0.05)
#define M_TORSO_Z (0.09)
#define L_TORSO_X (-0.0125)

#define L_CLAV_X (0.024)
#define L_CLAV_Y (0.221)
#define L_CLAV_Z (0.289)
#define L_SCAP_Y (0.0469519)    // these are derived from actual values (0.075, 0.036)
#define L_SCAP_Z (0.0686769)
#define L_UARM_Y (0.185)
#define L_LARM_Y (0.121)
#define L_LARM_Z (0.013)
#define L_FARM_Y (0.188)
#define L_FARM_Z (-0.013)
#define L_HAND_Y (0.058 + PALM_OFFSET)

#define R_CLAV_X (0.024)
#define R_CLAV_Y (-0.221)
#define R_CLAV_Z (0.289)
#define R_SCAP_Y (-0.0469519) // these are derived from actual values (0.075, 0.036)
#define R_SCAP_Z (0.0686769)
#define R_UARM_Y (-0.185)
#define R_LARM_Y (-0.121)
#define R_LARM_Z (0.013)
#define R_FARM_Y (-0.188)
#define R_FARM_Z (-0.013)
#define R_HAND_Y (-0.058 - PALM_OFFSET)

#define L_USY_ROT (-0.523598775)   // -30 deg. shoulder rotation
#define R_USY_ROT (0.523598775)   // 30 deg. shoulder rotation

// joint limits
#define LBZ_MAX (0.610865)
#define LBZ_MIN (-0.610865)
#define MBY_MAX (1.28)
#define MBY_MIN (-1.20)
#define UBX_MAX (1.)
#define UBX_MIN (-1.)
#define NECK_MAX (0.7854)
#define NECK_MIN (-0.7854)

#define L_USY_MAX (1.9635)
#define L_USY_MIN (-1.9635)
#define L_MSX_MAX (1.74533 + 0.523598775)
#define L_MSX_MIN (-1.39626 + 0.523598775)
#define L_ELY_MAX (3.14159)
#define L_ELY_MIN (-1.0e-6)
#define L_ELX_MAX (2.35619)
#define L_ELX_MIN (-1.0e-6)
#define L_UWY_MAX (1.571)
#define L_UWY_MIN (-1.571)
#define L_MWX_MAX (1.571)
#define L_MWX_MIN (-0.436)

#define R_USY_MAX (1.9635)
#define R_USY_MIN (-1.9635)
#define R_MSX_MAX (1.39626 - 0.523598775)
#define R_MSX_MIN (-1.74533 - 0.523598775)
#define R_ELY_MAX (3.14159)
#define R_ELY_MIN (-1.0e-6)
#define R_ELX_MAX (1.0e-6)
#define R_ELX_MIN (-2.35619)
#define R_UWY_MAX (1.571)
#define R_UWY_MIN (-1.571)
#define R_MWX_MAX (0.436)
#define R_MWX_MIN (-1.571)

#define L_UHZ_MAX (1.)
#define L_UHZ_MIN (-1.)
#define L_MHX_MAX (1.)
#define L_MHX_MIN (-1.)
#define L_LHY_MAX (1)
#define L_LHY_MIN (-1.)
#define L_KNY_MAX (1.)
#define L_KNY_MIN (-1.)
#define L_UAY_MAX (1.)
#define L_UAY_MIN (-1.)
#define L_LAX_MAX (1.)
#define L_LAX_MIN (-1.)

#define R_UHZ_MAX (1.)
#define R_UHZ_MIN (-1.)
#define R_MHX_MAX (1.)
#define R_MHX_MIN (-1.)
#define R_LHY_MAX (1)
#define R_LHY_MIN (-1.)
#define R_KNY_MAX (1.)
#define R_KNY_MIN (-1.)
#define R_UAY_MAX (1.)
#define R_UAY_MIN (-1.)
#define R_LAX_MAX (1.)
#define R_LAX_MIN (-1.)

#define D2R (M_PI/180.0)
#define R2D (180.0/M_PI)

#define MAX_ITERATIONS (5)
#define MIN_ERROR      (0.01)  
#define NUM_STEPS      (2251)

typedef struct {
  int valid[4];
  double angles[4][6];
  double error[4];
} SOLUTION_T;


int ik(int side, double tIn[12], double torso[9], int *numSolsP, double aOut[4][6], double error[4]);
int ikWrist(int side, double tIn[12], double torso[9], int *numSolsP, double aOut[4][6], double error[4]);
void poseToTransform(double c[6], double t[12]);
void printTransform(char nameP[], double t[12]) ;
void extractXYZ(double t[12], double xyz[2][3]);
void fk(int side, double a[9], double tOut[12]);
void fkToWrist(int side, double a[9], double tOut[12]);
