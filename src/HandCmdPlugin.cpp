/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include <ros/ros.h>
#include <re2uta_ocuPlugins/HandCmdPlugin.h>
#include <QHBoxLayout>
#include <QVBoxLayout>


HandCmdPlugin::HandCmdPlugin() : Super()
{
    // Slider and spin boxes
    for (int i=0; i<24; i++) {
        m_sliderSpin[i] = new SliderAndSpinBox();
        (m_sliderSpin[i])->setRange(-90, 90);
    }

    // hand labels
    m_sliderSpin[0]->setLabelTxt("L IND SPR");
    m_sliderSpin[1]->setLabelTxt("L IND PRX");
    m_sliderSpin[2]->setLabelTxt("L IND DST");
    m_sliderSpin[3]->setLabelTxt("L MID SPR");
    m_sliderSpin[4]->setLabelTxt("L MID PRX");
    m_sliderSpin[5]->setLabelTxt("L MID DST");
    m_sliderSpin[6]->setLabelTxt("L PNK SPR");
    m_sliderSpin[7]->setLabelTxt("L PNK PRX");
    m_sliderSpin[8]->setLabelTxt("L PNK DST");
    m_sliderSpin[9]->setLabelTxt("L THM SPR");
    m_sliderSpin[10]->setLabelTxt("L THM PRX");
    m_sliderSpin[11]->setLabelTxt("L THM DST");

    m_sliderSpin[12]->setLabelTxt("R IND SPR");
    m_sliderSpin[13]->setLabelTxt("R IND PRX");
    m_sliderSpin[14]->setLabelTxt("R IND DST");
    m_sliderSpin[15]->setLabelTxt("R MID SPR");
    m_sliderSpin[16]->setLabelTxt("R MID PRX");
    m_sliderSpin[17]->setLabelTxt("R MID DST");
    m_sliderSpin[18]->setLabelTxt("R PNK SPR");
    m_sliderSpin[19]->setLabelTxt("R PNK PRX");
    m_sliderSpin[20]->setLabelTxt("R PNK DST");
    m_sliderSpin[21]->setLabelTxt("R THM SPR");
    m_sliderSpin[22]->setLabelTxt("R THM PRX");
    m_sliderSpin[23]->setLabelTxt("R THM DST");

    // command / actual labels
    m_label[0] = new QLabel("CMD");
    m_label[1] = new QLabel("ACT");
    m_label[2] = new QLabel("CMD");
    m_label[3] = new QLabel("ACT");

    // blank labels
    for (int i=4; i<8; i++) {
        m_label[i] = new QLabel("");
    }

    // state
    for (int i=0; i<24; i++) {
        m_term[i] = new QDoubleSpinBox();
        (m_term[i])->setSingleStep(1);
        (m_term[i])->setDecimals(0);
        (m_term[i])->setRange(-90, 90);
    }

    // create the buttons
    m_button[0] = new QPushButton("UP LT", this);
    m_button[1] = new QPushButton("UP RT", this);
    m_button[2] = new QPushButton("HOSE 1", this);
    m_button[3] = new QPushButton("HOSE 3", this);
    m_button[4] = new QPushButton("SAVE P3", this);
    m_button[5] = new QPushButton("SAVE P4", this);
    m_button[6] = new QPushButton("SAVE P5", this);
    m_button[7] = new QPushButton("SAVE P6", this);
    m_button[8] = new QPushButton("SAVE P7", this);
    m_button[9] = new QPushButton("SAVE P8", this);
    m_button[10] = new QPushButton("SAVE P9", this);
    m_button[11] = new QPushButton("SAVE P10", this);
    m_button[12] = new QPushButton("SAVE P11", this);
    m_button[13] = new QPushButton("SEND LT", this);
    m_button[14] = new QPushButton("SEND RT", this);
    m_button[15] = new QPushButton("HOSE 2", this);
    m_button[16] = new QPushButton("HOSE 4", this);
    m_button[17] = new QPushButton("", this);
    m_button[18] = new QPushButton("", this);
    m_button[19] = new QPushButton("", this);
    m_button[20] = new QPushButton("", this);
    m_button[21] = new QPushButton("", this);
    m_button[22] = new QPushButton("", this);
    m_button[23] = new QPushButton("", this);
    m_button[24] = new QPushButton("", this);
    m_button[25] = new QPushButton("", this);

    connect(m_button[0], SIGNAL(released()), this, SLOT(updateLeftState()));
    connect(m_button[1], SIGNAL(released()), this, SLOT(updateRightState()));
    connect(m_button[2], SIGNAL(released()), this, SLOT(hose1()));
    connect(m_button[3], SIGNAL(released()), this, SLOT(hose3()));
    connect(m_button[4], SIGNAL(released()), this, SLOT(saveP3()));
    connect(m_button[5], SIGNAL(released()), this, SLOT(saveP4()));
    connect(m_button[6], SIGNAL(released()), this, SLOT(saveP5()));
    connect(m_button[7], SIGNAL(released()), this, SLOT(saveP6()));
    connect(m_button[8], SIGNAL(released()), this, SLOT(saveP7()));
    connect(m_button[9], SIGNAL(released()), this, SLOT(saveP8()));
    connect(m_button[10], SIGNAL(released()), this, SLOT(saveP9()));
    connect(m_button[11], SIGNAL(released()), this, SLOT(saveP10()));
    connect(m_button[12], SIGNAL(released()), this, SLOT(saveP11()));
    connect(m_button[13], SIGNAL(released()), this, SLOT(sendLeftCmd()));
    connect(m_button[14], SIGNAL(released()), this, SLOT(sendRightCmd()));
    connect(m_button[15], SIGNAL(released()), this, SLOT(hose2()));
    connect(m_button[16], SIGNAL(released()), this, SLOT(hose4()));
    connect(m_button[17], SIGNAL(released()), this, SLOT(doP3()));
    connect(m_button[18], SIGNAL(released()), this, SLOT(doP4()));
    connect(m_button[19], SIGNAL(released()), this, SLOT(doP5()));
    connect(m_button[20], SIGNAL(released()), this, SLOT(doP6()));
    connect(m_button[21], SIGNAL(released()), this, SLOT(doP7()));
    connect(m_button[22], SIGNAL(released()), this, SLOT(doP8()));
    connect(m_button[23], SIGNAL(released()), this, SLOT(doP9()));
    connect(m_button[24], SIGNAL(released()), this, SLOT(doP10()));
    connect(m_button[25], SIGNAL(released()), this, SLOT(doP11()));

    int numHBox = 13;
    QHBoxLayout *hbox[numHBox];
    QVBoxLayout *vbox = new QVBoxLayout(this);

    for (int i=0; i<numHBox; i++) {
        hbox[i] = new QHBoxLayout();
    }

    // TITLE BOX
    (hbox[0])->addWidget(m_label[4]);
    (hbox[0])->addWidget(m_label[5]);
    (hbox[0])->addWidget(m_label[0]);
    (hbox[0])->addWidget(m_label[1]);
    (hbox[0])->addWidget(m_label[6]);
    (hbox[0])->addWidget(m_label[7]);
    (hbox[0])->addWidget(m_label[2]);
    (hbox[0])->addWidget(m_label[3]);
    (hbox[0])->addWidget(m_button[0]);
    (hbox[0])->addWidget(m_button[13]);

    (hbox[1])->addWidget(m_sliderSpin[0]);
    (hbox[1])->addWidget(m_term[0]);
    (hbox[1])->addWidget(m_sliderSpin[12]);
    (hbox[1])->addWidget(m_term[12]);
    (hbox[1])->addWidget(m_button[1]);
    (hbox[1])->addWidget(m_button[14]);

    (hbox[2])->addWidget(m_sliderSpin[3]);
    (hbox[2])->addWidget(m_term[3]);
    (hbox[2])->addWidget(m_sliderSpin[15]);
    (hbox[2])->addWidget(m_term[15]);
    (hbox[2])->addWidget(m_button[2]);
    (hbox[2])->addWidget(m_button[15]);

    (hbox[3])->addWidget(m_sliderSpin[6]);
    (hbox[3])->addWidget(m_term[6]);
    (hbox[3])->addWidget(m_sliderSpin[18]);
    (hbox[3])->addWidget(m_term[18]);
    (hbox[3])->addWidget(m_button[3]);
    (hbox[3])->addWidget(m_button[16]);

    (hbox[4])->addWidget(m_sliderSpin[9]);
    (hbox[4])->addWidget(m_term[9]);
    (hbox[4])->addWidget(m_sliderSpin[21]);
    (hbox[4])->addWidget(m_term[21]);
    (hbox[4])->addWidget(m_button[4]);
    (hbox[4])->addWidget(m_button[17]);

    (hbox[5])->addWidget(m_sliderSpin[1]);
    (hbox[5])->addWidget(m_term[1]);
    (hbox[5])->addWidget(m_sliderSpin[13]);
    (hbox[5])->addWidget(m_term[13]);
    (hbox[5])->addWidget(m_button[5]);
    (hbox[5])->addWidget(m_button[18]);

    (hbox[6])->addWidget(m_sliderSpin[4]);
    (hbox[6])->addWidget(m_term[4]);
    (hbox[6])->addWidget(m_sliderSpin[16]);
    (hbox[6])->addWidget(m_term[16]);
    (hbox[6])->addWidget(m_button[6]);
    (hbox[6])->addWidget(m_button[19]);

    (hbox[7])->addWidget(m_sliderSpin[7]);
    (hbox[7])->addWidget(m_term[7]);
    (hbox[7])->addWidget(m_sliderSpin[19]);
    (hbox[7])->addWidget(m_term[19]);
    (hbox[7])->addWidget(m_button[7]);
    (hbox[7])->addWidget(m_button[20]);

    (hbox[8])->addWidget(m_sliderSpin[10]);
    (hbox[8])->addWidget(m_term[10]);
    (hbox[8])->addWidget(m_sliderSpin[22]);
    (hbox[8])->addWidget(m_term[22]);
    (hbox[8])->addWidget(m_button[8]);
    (hbox[8])->addWidget(m_button[21]);

    (hbox[9])->addWidget(m_sliderSpin[2]);
    (hbox[9])->addWidget(m_term[2]);
    (hbox[9])->addWidget(m_sliderSpin[14]);
    (hbox[9])->addWidget(m_term[14]);
    (hbox[9])->addWidget(m_button[9]);
    (hbox[9])->addWidget(m_button[22]);

    (hbox[10])->addWidget(m_sliderSpin[5]);
    (hbox[10])->addWidget(m_term[5]);
    (hbox[10])->addWidget(m_sliderSpin[17]);
    (hbox[10])->addWidget(m_term[17]);
    (hbox[10])->addWidget(m_button[10]);
    (hbox[10])->addWidget(m_button[23]);

    (hbox[11])->addWidget(m_sliderSpin[8]);
    (hbox[11])->addWidget(m_term[8]);
    (hbox[11])->addWidget(m_sliderSpin[20]);
    (hbox[11])->addWidget(m_term[20]);
    (hbox[11])->addWidget(m_button[11]);
    (hbox[11])->addWidget(m_button[24]);

    (hbox[12])->addWidget(m_sliderSpin[11]);
    (hbox[12])->addWidget(m_term[11]);
    (hbox[12])->addWidget(m_sliderSpin[23]);
    (hbox[12])->addWidget(m_term[23]);
    (hbox[12])->addWidget(m_button[12]);
    (hbox[12])->addWidget(m_button[25]);


    for (int i=0; i<numHBox; i++) {
        vbox->addLayout(hbox[i]);
    }

    show();

    // Presets
    for (int i=0; i<2; i++) {
        for (int j=0; j<11; j++) {
            m_presetValid[i] = false;
            (m_presets[i][j]).position.resize(12);
        }
    }

    m_leftHandCmdPub = m_node.advertise< re2uta_inetComms::JointCommand >( "/ui/left_hand_cmds", 1, this);
    m_rightHandCmdPub = m_node.advertise< re2uta_inetComms::JointCommand >( "/ui/right_hand_cmds", 1, this);

    m_reqLeftHandStatePub = m_node.advertise< std_msgs::Empty >("/ui/leftHandStateHandlerReq", 1, this);
    m_leftHandStateSub = m_node.subscribe("/ui/leftHandStateHandlerData", 1, &HandCmdPlugin::leftHandStateCallback, this);

    m_reqRightHandStatePub = m_node.advertise< std_msgs::Empty >("/ui/rightHandStateHandlerReq", 1, this);
    m_rightHandStateSub = m_node.subscribe("/ui/rightHandStateHandlerData", 1, &HandCmdPlugin::rightHandStateCallback, this);

    m_leftHandCmdMsg.valid.resize(12);
    m_leftHandCmdMsg.cmd.position.resize(12);

    m_rightHandCmdMsg.valid.resize(12);
    m_rightHandCmdMsg.cmd.position.resize(12);

    for (int i=0; i<12; i++) {
        m_leftHandCmdMsg.valid[i] = 0;
        m_leftHandCmdMsg.cmd.position[i] = 0;
        m_rightHandCmdMsg.valid[i] = 0;
        m_rightHandCmdMsg.cmd.position[i] = 0;
    }

    m_currentLeftHand.position.resize(12);
    m_currentRightHand.position.resize(12);
}

HandCmdPlugin::~HandCmdPlugin()
{
}

// CALLBACKS
void HandCmdPlugin::leftHandStateCallback(const sensor_msgs::JointState::ConstPtr& msg)
{
    m_currentLeftHand.position = msg->position;

    // show the current angles
    for (int i=0; i<12; i++) {
        (m_term[i])->setValue(m_currentLeftHand.position[i]*R2D);
    }
}

void HandCmdPlugin::rightHandStateCallback(const sensor_msgs::JointState::ConstPtr& msg)
{
    m_currentRightHand.position = msg->position;

    // show the current angles
    for (int i=0; i<12; i++) {
        (m_term[i+12])->setValue(m_currentRightHand.position[i]*R2D);
    }
}

// BUTTON PRESSES
void HandCmdPlugin::updateLeftState()
{
    std_msgs::Empty msg;

    m_reqLeftHandStatePub.publish(msg);
}

void HandCmdPlugin::sendLeftCmd()
{
    ROS_INFO("IN SEND LEFT CMD");
    // check to see if any commands have changed from the last send
    for (int i=0; i<12; i++) {
        if (fabs( (m_sliderSpin[i])->getSpinBoxVal()*D2R - m_leftHandCmdMsg.cmd.position[i] ) > CMD_CHANGE) {
            ROS_INFO("VALID CHANGE");
            ROS_INFO("BEFORE");
            ROS_INFO("%.2f", m_leftHandCmdMsg.cmd.position[i]);
            ROS_INFO("%.2f", (m_sliderSpin[i])->getSpinBoxVal());
            ROS_INFO("%.2f", (m_sliderSpin[i])->getSliderVal());
            m_leftHandCmdMsg.cmd.position[i] = (m_sliderSpin[i])->getSpinBoxVal()*D2R;
            ROS_INFO("AFTER");
            ROS_INFO("%.2f", m_leftHandCmdMsg.cmd.position[i]);
            ROS_INFO("%.2f", (m_sliderSpin[i])->getSpinBoxVal());
            ROS_INFO("%.2f", (m_sliderSpin[i])->getSliderVal());
            m_leftHandCmdMsg.valid[i] = 1;
        } else {
            ROS_INFO("NO CHANGE");
            ROS_INFO("%.2f", m_leftHandCmdMsg.cmd.position[i]);
            ROS_INFO("%.2f", (m_sliderSpin[i])->getSpinBoxVal());
            ROS_INFO("%.2f", (m_sliderSpin[i])->getSliderVal());
            m_leftHandCmdMsg.valid[i] = 0;
        }
    }

    ROS_INFO("SENDING LEFT HAND CMD");
    m_leftHandCmdPub.publish(m_leftHandCmdMsg);
}

void HandCmdPlugin::updateRightState()
{
    std_msgs::Empty msg;

    m_reqRightHandStatePub.publish(msg);
}

void HandCmdPlugin::sendRightCmd()
{
    // check to see if any commands have changed from the last send
    for (int i=0; i<12; i++) {
//        ROS_INFO("%d: %g %g", i, (m_term[2*i])->value(), m_jointCmdMsg.cmd.position[i]*R2D);
        if (fabs( (m_sliderSpin[i+12])->getSpinBoxVal()*D2R - m_rightHandCmdMsg.cmd.position[i] ) > CMD_CHANGE) {
//            ROS_INFO("VALID CHANGE");
            m_rightHandCmdMsg.cmd.position[i] = (m_sliderSpin[i+12])->getSpinBoxVal()*D2R;
            m_rightHandCmdMsg.valid[i] = 1;
        } else {
//            ROS_INFO("NO CHANGE");
            m_rightHandCmdMsg.valid[i] = 0;
        }
    }

    m_rightHandCmdPub.publish(m_rightHandCmdMsg);
}

void HandCmdPlugin::hose1()
{
  (m_sliderSpin[21])->setSliderVal(20);
  (m_sliderSpin[22])->setSliderVal(45);
  (m_sliderSpin[23])->setSliderVal(20);
}

void HandCmdPlugin::hose3()
{
  (m_sliderSpin[13])->setSliderVal(90);
  (m_sliderSpin[16])->setSliderVal(90);
  (m_sliderSpin[19])->setSliderVal(90);
}

void HandCmdPlugin::saveP3()
{
    for (int i=0; i<12; i++) {
        (m_presets[0][2]).position[i] = (m_sliderSpin[i])->getSpinBoxVal()*D2R;    
        (m_presets[1][2]).position[i] = (m_sliderSpin[i+12])->getSpinBoxVal()*D2R;    
    }
    m_presetValid[2] = true;
    (m_button[17])->setText("PRESET 3");
}

void HandCmdPlugin::saveP4()
{
    for (int i=0; i<12; i++) {
        (m_presets[0][3]).position[i] = (m_sliderSpin[i])->getSpinBoxVal()*D2R;    
        (m_presets[1][3]).position[i] = (m_sliderSpin[i+12])->getSpinBoxVal()*D2R;    
    }
    m_presetValid[3] = true;
    (m_button[18])->setText("PRESET 4");
}

void HandCmdPlugin::saveP5()
{
    for (int i=0; i<12; i++) {
        (m_presets[0][4]).position[i] = (m_sliderSpin[i])->getSpinBoxVal()*D2R;    
        (m_presets[1][4]).position[i] = (m_sliderSpin[i+12])->getSpinBoxVal()*D2R;    
    }
    m_presetValid[4] = true;
    (m_button[19])->setText("PRESET 5");
}

void HandCmdPlugin::saveP6()
{
    for (int i=0; i<12; i++) {
        (m_presets[0][5]).position[i] = (m_sliderSpin[i])->getSpinBoxVal()*D2R;    
        (m_presets[1][5]).position[i] = (m_sliderSpin[i+12])->getSpinBoxVal()*D2R;    
    }
    m_presetValid[5] = true;
    (m_button[20])->setText("PRESET 6");
}

void HandCmdPlugin::saveP7()
{
    for (int i=0; i<12; i++) {
        (m_presets[0][6]).position[i] = (m_sliderSpin[i])->getSpinBoxVal()*D2R;    
        (m_presets[1][6]).position[i] = (m_sliderSpin[i+12])->getSpinBoxVal()*D2R;    
    }
    m_presetValid[6] = true;
    (m_button[21])->setText("PRESET 7");
}

void HandCmdPlugin::saveP8()
{
    for (int i=0; i<12; i++) {
        (m_presets[0][7]).position[i] = (m_sliderSpin[i])->getSpinBoxVal()*D2R;    
        (m_presets[1][7]).position[i] = (m_sliderSpin[i+12])->getSpinBoxVal()*D2R;    
    }
    m_presetValid[7] = true;
    (m_button[22])->setText("PRESET 8");
}

void HandCmdPlugin::saveP9()
{
    for (int i=0; i<12; i++) {
        (m_presets[0][8]).position[i] = (m_sliderSpin[i])->getSpinBoxVal()*D2R;    
        (m_presets[1][8]).position[i] = (m_sliderSpin[i+12])->getSpinBoxVal()*D2R;    
    }
    m_presetValid[8] = true;
    (m_button[23])->setText("PRESET 9");
}

void HandCmdPlugin::saveP10()
{
    for (int i=0; i<12; i++) {
        (m_presets[0][9]).position[i] = (m_sliderSpin[i])->getSpinBoxVal()*D2R;    
        (m_presets[1][9]).position[i] = (m_sliderSpin[i+12])->getSpinBoxVal()*D2R;    
    }
    m_presetValid[9] = true;
    (m_button[24])->setText("PRESET 10");
}

void HandCmdPlugin::saveP11()
{
    for (int i=0; i<12; i++) {
        (m_presets[0][10]).position[i] = (m_sliderSpin[i])->getSpinBoxVal()*D2R;    
        (m_presets[1][10]).position[i] = (m_sliderSpin[i+12])->getSpinBoxVal()*D2R;    
    }
    m_presetValid[10] = true;
    (m_button[25])->setText("PRESET 11");
}

void HandCmdPlugin::hose2()
{
  (m_sliderSpin[14])->setSliderVal(45);
  (m_sliderSpin[17])->setSliderVal(45);
  (m_sliderSpin[20])->setSliderVal(45);
}

void HandCmdPlugin::hose4()
{
  (m_sliderSpin[22])->setSliderVal(90);
}

void HandCmdPlugin::doP3()
{
    if (m_presetValid[2] == true) {
        for (int i=0; i<12; i++) {
            (m_sliderSpin[i])->setSliderVal((int) ((m_presets[0][2]).position[i]*R2D));
            (m_sliderSpin[i+12])->setSliderVal((int) ((m_presets[1][2]).position[i]*R2D));
        }
    }
}

void HandCmdPlugin::doP4()
{
    if (m_presetValid[3] == true) {
        for (int i=0; i<12; i++) {
            (m_sliderSpin[i])->setSliderVal((int) ((m_presets[0][3]).position[i]*R2D));
            (m_sliderSpin[i+12])->setSliderVal((int) ((m_presets[1][3]).position[i]*R2D));
        }
    }
}

void HandCmdPlugin::doP5()
{
    if (m_presetValid[4] == true) {
        for (int i=0; i<12; i++) {
            (m_sliderSpin[i])->setSliderVal((int) ((m_presets[0][4]).position[i]*R2D));
            (m_sliderSpin[i+12])->setSliderVal((int) ((m_presets[1][4]).position[i]*R2D));
        }
    }
}

void HandCmdPlugin::doP6()
{
    if (m_presetValid[5] == true) {
        for (int i=0; i<12; i++) {
            (m_sliderSpin[i])->setSliderVal((int) ((m_presets[0][5]).position[i]*R2D));
            (m_sliderSpin[i+12])->setSliderVal((int) ((m_presets[1][5]).position[i]*R2D));
        }
    }
}

void HandCmdPlugin::doP7()
{
    if (m_presetValid[6] == true) {
        for (int i=0; i<12; i++) {
            (m_sliderSpin[i])->setSliderVal((int) ((m_presets[0][6]).position[i]*R2D));
            (m_sliderSpin[i+12])->setSliderVal((int) ((m_presets[1][6]).position[i]*R2D));
        }
    }
}

void HandCmdPlugin::doP8()
{
    if (m_presetValid[7] == true) {
        for (int i=0; i<12; i++) {
            (m_sliderSpin[i])->setSliderVal((int) ((m_presets[0][7]).position[i]*R2D));
            (m_sliderSpin[i+12])->setSliderVal((int) ((m_presets[1][7]).position[i]*R2D));
        }
    }
}

void HandCmdPlugin::doP9()
{
    if (m_presetValid[8] == true) {
        for (int i=0; i<12; i++) {
            (m_sliderSpin[i])->setSliderVal((int) ((m_presets[0][8]).position[i]*R2D));
            (m_sliderSpin[i+12])->setSliderVal((int) ((m_presets[1][8]).position[i]*R2D));
        }
    }
}

void HandCmdPlugin::doP10()
{
    if (m_presetValid[9] == true) {
        for (int i=0; i<12; i++) {
            (m_sliderSpin[i])->setSliderVal((int) ((m_presets[0][9]).position[i]*R2D));
            (m_sliderSpin[i+12])->setSliderVal((int) ((m_presets[1][9]).position[i]*R2D));
        }
    }
}

void HandCmdPlugin::doP11()
{
    if (m_presetValid[10] == true) {
        for (int i=0; i<12; i++) {
            (m_sliderSpin[i])->setSliderVal((int) ((m_presets[0][10]).position[i]*R2D));
            (m_sliderSpin[i+12])->setSliderVal((int) ((m_presets[1][10]).position[i]*R2D));
        }
    }
}

/**
 * Required for rviz plug-in
 */
void HandCmdPlugin::saveToConfig( const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config )
{
}
/**
 * Required for rviz plug-in
 */
void HandCmdPlugin::loadFromConfig( const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config )
{
}

/**
 * Required for rviz plug-in
 */
void HandCmdPlugin::onInitialize()
{
}

// Tell pluginlib about this class.  Every class which should be
// loadable by pluginlib::ClassLoader must have these two lines
// compiled in its .cpp file, outside of any namespace scope.
#include <pluginlib/class_list_macros.h>
PLUGINLIB_DECLARE_CLASS( re2uta_ocuPlugins, Hands, HandCmdPlugin, rviz::Panel )


