
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <re2uta_ocuPlugins/armik.h>

// a * b = c
void multiplyTransform(double a[12], double b[12], double c[12])
{
    c[0] = a[0]*b[0] + a[1]*b[4] + a[2]*b[8];
    c[1] = a[0]*b[1] + a[1]*b[5] + a[2]*b[9];
    c[2] = a[0]*b[2] + a[1]*b[6] + a[2]*b[10];
    c[3] = a[0]*b[3] + a[1]*b[7] + a[2]*b[11] + a[3];
    
    c[4] = a[4]*b[0] + a[5]*b[4] + a[6]*b[8];
    c[5] = a[4]*b[1] + a[5]*b[5] + a[6]*b[9];
    c[6] = a[4]*b[2] + a[5]*b[6] + a[6]*b[10];
    c[7] = a[4]*b[3] + a[5]*b[7] + a[6]*b[11] + a[7];
    
    c[8]  = a[8]*b[0] + a[9]*b[4] + a[10]*b[8];
    c[9]  = a[8]*b[1] + a[9]*b[5] + a[10]*b[9];
    c[10] = a[8]*b[2] + a[9]*b[6] + a[10]*b[10];
    c[11] = a[8]*b[3] + a[9]*b[7] + a[10]*b[11] + a[11];
}

void invertTransform(double a[12], double aInv[12])
{
    aInv[0] = a[0];
    aInv[1] = a[4];
    aInv[2] = a[8];
    aInv[4] = a[1];
    aInv[5] = a[5];
    aInv[6] = a[9];
    aInv[8] = a[2];
    aInv[9] = a[6];
    aInv[10] = a[10];
    
    aInv[3] = a[0]*-a[3] + a[4]*-a[7] + a[8]*-a[11];
    aInv[7] = a[1]*-a[3] + a[5]*-a[7] + a[9]*-a[11];
    aInv[11] = a[2]*-a[3] + a[6]*-a[7] + a[10]*-a[11];
}

void printTransform(char nameP[], double t[12]) 
{
    if (nameP) {
        printf("%s\n", nameP);
    }
    printf("%.4f %.4f %.4f %.4f\n", t[0], t[1], t[2], t[3]);
    printf("%.4f %.4f %.4f %.4f\n", t[4], t[5], t[6], t[7]);
    printf("%.4f %.4f %.4f %.4f\n", t[8], t[9], t[10], t[11]);
}  

void extractXYZ(double t[12], double xyz[2][3])
{
    double q1[3], q2[3];
    
    q1[1] = atan2(-t[8], sqrt(t[0]*t[0]+t[4]*t[4]));

    if (fabs((fabs(q1[1])-(M_PI/2))) < 1.0e-4) {
        q1[0] = atan2(t[1], t[5]);
        q1[1] = M_PI/2.0;
        q1[2] = 0.0;
    } else {
        q1[0] = atan2(t[9]/cos(q1[1]), t[10]/cos(q1[1]));
        q1[2] = atan2(t[4]/cos(q1[1]), t[0]/cos(q1[1]));
    }

    q2[1] = atan2(-t[8], -sqrt(t[0]*t[0]+t[4]*t[4]));

    if (fabs((fabs(q2[1])-(M_PI/2))) < 1.0e-4) {
        q2[0] = -atan2(t[1], t[5]);
        q2[1] = -M_PI/2.0;
        q2[2] = 0.0;
    } else {
        q2[0] = atan2(t[9]/cos(q1[1]), t[10]/cos(q1[1]));
        q2[2] = atan2(t[4]/cos(q1[1]), t[0]/cos(q1[1]));
    }

    xyz[0][0] = q1[0];
    xyz[0][1] = q1[1];
    xyz[0][2] = q1[2];

    xyz[1][0] = q2[0];
    xyz[1][1] = q2[1];
    xyz[1][2] = q2[2];
}

void poseToTransform(double c[6], double t[12])
{
    double ca = cos(c[5]);
    double sa = sin(c[5]);
    double cb = cos(c[4]);
    double sb = sin(c[4]);
    double cc = cos(c[3]);
    double sc = sin(c[3]);
    
    t[0] = ca*cb;
    t[1] = ca*sb*sc - sa*cc;
    t[2] = ca*sb*cc + sa*sc;
    t[3] = c[0];
    
    t[4] = sa*cb;
    t[5] = sa*sb*sc + ca*cc;
    t[6] = sa*sb*cc - ca*sc;
    t[7] = c[1];
    
    t[8] = -sb;
    t[9] = cb*sc;
    t[10] = cb*cc;
    t[11] = c[2];
}

void fkToWrist(int side, double a[9], double t[12]);

void fk(int side, double a[9], double tOut[12])
{
    double t[12];
    double t09[12];
    double t10[12];
    double t12[12];
    double t23[12];
    double t34[12];

    fkToWrist(side, a, t);

    // lower arm to forearm (upper wrist y joint)
    t12[0] = cos(a[7]);
    t12[1] = 0;
    t12[2] = sin(a[7]);
    t12[3] = 0;
    t12[4] = 0;
    t12[5] = 1;
    t12[6] = 0;
    t12[7] = 0;
    t12[8] = -sin(a[7]);
    t12[9] = 0;
    t12[10] = cos(a[7]);
    t12[11] = 0;
    
    // multiply to get pelvis to forearm
    multiplyTransform(t, t12, t09);
    
#if 0
    printTransform("FK TP-FA", t09);
#endif
    
    // forearm to hand (middle wrist x joint)
    t23[0] = 1;
    t23[1] = 0;
    t23[2] = 0;
    t23[3] = 0;
    t23[4] = 0;
    t23[5] = cos(a[8]);
    t23[6] = -sin(a[8]);
    t23[7] = 0;
    t23[8] = 0;
    t23[9] = sin(a[8]);
    t23[10] = cos(a[8]);
    t23[11] = 0;
    
    multiplyTransform(t09, t23, t10);

#if 0
    printTransform("FK TP-H", t10);
#endif
    
    t34[0] = 1;
    t34[1] = 0;
    t34[2] = 0;
    t34[3] = 0;
    t34[4] = 0;
    t34[5] = 1;
    t34[6] = 0;
    if (side == LEFT) {
        t34[7] = L_HAND_Y;
    } else {
        t34[7] = R_HAND_Y;
    }
    t34[8] = 0;
    t34[9] = 0;
    t34[10] = 1;
    t34[11] = 0;
    
    // multiply to get pelvis to hand
    multiplyTransform(t10, t34, tOut);

#if 0
    printTransform("FK TFINAL", tOut);
#endif

}

void fkTorso(int side, double a[9], double tOut[12])
{
    double t01[12];
    double t02[12];
    double t03[12];
    double t12[12];
    double t23[12];
    double t34[12];
    
    // pelvis to lower torso (lower back z joint)
    t01[0] = cos(a[0]);
    t01[1] = -sin(a[0]);
    t01[2] = 0;
    t01[3] = L_TORSO_X;
    t01[4] = sin(a[0]);
    t01[5] = cos(a[0]);
    t01[6] = 0;
    t01[7] = 0;
    t01[8] = 0;
    t01[9] = 0;
    t01[10] = 1;
    t01[11] = 0;
    
#if 0
    printTransform("FK TP-LB", t01);
#endif
    
    // ltorso to mid torso (middle back y joint)
    t12[0] = cos(a[1]);
    t12[1] = 0;
    t12[2] = sin(a[1]);
    t12[3] = 0;
    t12[4] = 0;
    t12[5] = 1;
    t12[6] = 0;
    t12[7] = 0;
    t12[8] = -sin(a[1]);
    t12[9] = 0;
    t12[10] = cos(a[1]);
    t12[11] = M_TORSO_Z;
    
    // multiply to get pelvis to middle back
    multiplyTransform(t01, t12, t02);
    
#if 0
    printTransform("FK TP-MB", t02);
#endif
    
    // middle back to upper back (upper back x joint)
    t23[0] = 1;
    t23[1] = 0;
    t23[2] = 0;
    t23[3] = 0;
    t23[4] = 0;
    t23[5] = cos(a[2]);
    t23[6] = -sin(a[2]);
    t23[7] = 0;
    t23[8] = 0;
    t23[9] = sin(a[2]);
    t23[10] = cos(a[2]);
    t23[11] = U_TORSO_Z;
    
    // multiply to get pelvis to upper back
    multiplyTransform(t02, t23, t03);
    
#if 0
    printTransform("FK P-UB", t03);
#endif

    // upper back to clav origin
    t34[0] = 1;
    t34[1] = 0;
    t34[2] = 0;
    t34[4] = 0;
    t34[8] = 0;
    if (side == LEFT) {
        t34[3] = L_CLAV_X;
        t34[5] = cos(L_USY_ROT);
        t34[6] = -sin(L_USY_ROT);
        t34[7] = L_CLAV_Y;
        t34[9] = sin(L_USY_ROT);
        t34[10] = cos(L_USY_ROT);
        t34[11] = L_CLAV_Z;
    } else {
        t34[3] = R_CLAV_X;
        t34[5] = cos(R_USY_ROT);
        t34[6] = -sin(R_USY_ROT);
        t34[7] = R_CLAV_Y;
        t34[9] = sin(R_USY_ROT);
        t34[10] = cos(R_USY_ROT);
        t34[11] = R_CLAV_Z;
    }

    // multiply to get pelvis to clav origin
    multiplyTransform(t03, t34, tOut);
    
#if 0
    printTransform("FK TP-Co", tOut);
#endif
}

void fkToWrist(int side, double a[9], double tOut[12])
{
    double tTorso[12];
    double t05[12];
    double t06[12];
    double t07[12];
    double t08[12];
    double t45[12];
    double t56[12];
    double t67[12];
    double t78[12];
    double t89[12];
#if 0    
    printf("IN FK TO WRIST: %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f\n",
             a[0]*R2D,
             a[1]*R2D,
             a[2]*R2D,
             a[3]*R2D,
             a[4]*R2D,
             a[5]*R2D,
             a[6]*R2D,
             a[7]*R2D,
             a[8]*R2D);
#endif

    fkTorso(side, a, tTorso);

    // clav origin to clav (upper shoulder y joint - really rotates about z)
    t45[0] = cos(a[3]);
    t45[1] = -sin(a[3]);
    t45[2] = 0;
    t45[3] = 0;
    t45[4] = sin(a[3]);
    t45[5] = cos(a[3]);
    t45[6] = 0;
    t45[7] = 0;
    t45[8] = 0;
    t45[9] = 0;
    t45[10] = 1;
    t45[11] = 0;
    
    // multiply to get pelvis to clav
    multiplyTransform(tTorso, t45, t05);
    
#if 0
    printTransform("FK TP-C", t05);
#endif

    // clav to scap (shoulder x joint)
    t56[0] = 1;
    t56[1] = 0;
    t56[2] = 0;
    t56[3] = 0;
    t56[4] = 0;
    t56[5] = cos(a[4]);
    t56[6] = -sin(a[4]);
    t56[8] = 0;
    t56[9] = sin(a[4]);
    t56[10] = cos(a[4]);
    if (side == LEFT) {
        t56[7] = L_SCAP_Y;
        t56[11] = L_SCAP_Z;
    } else {
        t56[7] = R_SCAP_Y;
        t56[11] = R_SCAP_Z;
    }
    
    // multiply to get pelvis to scap
    multiplyTransform(t05, t56, t06);
    
#if 0
    printTransform("FK TP-S", t06);
#endif
    
    // scap to upper arm (elbow y joint)
    t67[0] = cos(a[5]);
    t67[1] = 0;
    t67[2] = sin(a[5]);
    t67[3] = 0;
    t67[4] = 0;
    t67[5] = 1;
    t67[6] = 0;
    if (side == LEFT) {
        t67[7] = L_UARM_Y;
    } else {
        t67[7] = R_UARM_Y;
    }
    t67[8] = -sin(a[5]);
    t67[9] = 0;
    t67[10] = cos(a[5]);
    t67[11] = 0;
    
    // multiply to get pelvis to upper arm
    multiplyTransform(t06, t67, t07);
    
#if 0
    printTransform("FK TP-UA", t07);
#endif
    
    // upper arm to lower arm (elbow x joint)
    t78[0] = 1;
    t78[1] = 0;
    t78[2] = 0;
    t78[3] = 0;
    t78[4] = 0;
    t78[5] = cos(a[6]);
    t78[6] = -sin(a[6]);
    t78[8] = 0;
    t78[9] = sin(a[6]);
    t78[10] = cos(a[6]);
    if (side == LEFT) {
        t78[7] = L_LARM_Y;
        t78[11] = L_LARM_Z;
    } else {
        t78[7] = R_LARM_Y;
        t78[11] = R_LARM_Z;
    }
    
    // multiply to get pelvis to lower arm
    multiplyTransform(t07, t78, t08);

#if 0
    printTransform("FK TP-08", t08);
#endif

    t89[0] = 1;
    t89[1] = 0;
    t89[2] = 0;
    t89[3] = 0;
    t89[4] = 0;
    t89[5] = 1;
    t89[6] = 0;
    t89[8] = 0;
    t89[9] = 0;
    t89[10] = 1;
    if (side == LEFT) {
        t89[7] = L_FARM_Y;
        t89[11] = L_FARM_Z;
    } else {
        t89[7] = R_FARM_Y;
        t89[11] = R_FARM_Z;
    }

    multiplyTransform(t08, t89, tOut);

#if 0
    printTransform("FK TP-0ut", tOut);
#endif

}
    
int ik(int side, double tIn[12], double torso[9], int *numSolsP, double aOut[4][6], double error[4])
{
    double t16[12];
    double t1Wrist[12];
    double t45[12];
    double t4Wrist[12];
    double t56[12];
    double t6wrist[12];
    double tTorso[12];
    double tSoFar[12];
    double tSoFarInv[12];
    int    solutionIndex = 0;
    int    validSolution = 0;
    double solutionError = 1e10;
    SOLUTION_T solutions[NUM_STEPS];
//    int loopCtr = 1;
    double shoulderSearchStartAngle = -112.0 * D2R;
    double shoulderSearchResolution = 0.1 * D2R;

#if 0
    printTransform("tIn", tIn);
#endif

        
    // Step 1: Search across the upper shoulder y joint 
    for (int i=0; i<NUM_STEPS; i++) {
        for (int j=0; j<4; j++) {
            solutions[i].angles[j][0] = (i * shoulderSearchResolution + shoulderSearchStartAngle);
        }
            
#if 0
        printf("\nITERATION %d  RAW USY: %g\n", i,
               solutions[i].angles[0][0]*R2D);
#endif
        
        // Step 2: Remove the torso angles and upper shoulder y transform and offset 
        double tTemp1[12];
        double tTemp2[12];
        double tTorsoInv[12];
        double t54[12];
        double t65[12];
        
#if 0
        if (i == 0) {
            printf("TORSO: %.2f %.2f %.2f\n", 
                   torso[0]*R2D,
                   torso[1]*R2D,
                   torso[2]*R2D);
        }
#endif
        fkTorso(side, torso, tTorso);
        
        invertTransform(tTorso, tTorsoInv);
        
        multiplyTransform(tTorsoInv, tIn, tTemp1);
        
#if 0
        if (i == 112) {
            printTransform("tTemp1", tTemp1);
        }
#endif
        
        // clav origin to clav (upper shoulder y joint - really rotates about z)
        t45[0] = cos(solutions[i].angles[0][0]);
        t45[1] = -sin(solutions[i].angles[0][0]);
        t45[2] = 0;
        t45[3] = 0;
        t45[4] = sin(solutions[i].angles[0][0]);
        t45[5] = cos(solutions[i].angles[0][0]);
        t45[6] = 0;
        t45[7] = 0;
        t45[8] = 0;
        t45[9] = 0;
        t45[10] = 1;
        t45[11] = 0;
        
        invertTransform(t45, t54);
        
        multiplyTransform(t54, tTemp1, tTemp2);
        
        // clav to scap (translation only)
        t56[0] = 1;
        t56[1] = 0;
        t56[2] = 0;
        t56[3] = 0;
        t56[4] = 0;
        t56[5] = 1;
        t56[6] = 0;
        t56[8] = 0;
        t56[9] = 0;
        t56[10] = 1;
        if (side == LEFT) {
            t56[7] = L_SCAP_Y;
            t56[11] = L_SCAP_Z;
        } else {
            t56[7] = R_SCAP_Y;
            t56[11] = R_SCAP_Z;
        }
        
        invertTransform(t56, t65);
        
        multiplyTransform(t65, tTemp2, t16);
        
#if 0
        if (i == 112) {
            printTransform("t16", t16);
        }
#endif
        
        // Step 3: Get rid of the L_HAND_Y link length
        t6wrist[0] = 1;
        t6wrist[1] = 0;
        t6wrist[2] = 0;
        t6wrist[3] = 0;
        t6wrist[4] = 0;
        t6wrist[5] = 1;
        t6wrist[6] = 0;
        if (side == LEFT) {
            t6wrist[7] = -L_HAND_Y;
        } else {
            t6wrist[7] = -R_HAND_Y;
        } 
        t6wrist[8] = 0;
        t6wrist[9] = 0;
        t6wrist[10] = 1;
        t6wrist[11] = 0;
        
        multiplyTransform(t16, t6wrist, t1Wrist);
        
#if 0
        if (i == 112) {
            printTransform("t1Wrist", t1Wrist);
        }
#endif
        
        // Compute ELX for "false arm" using law of cosines
        double l1, l2;
        if (side == LEFT) {
            l1 = sqrt((L_UARM_Y+L_LARM_Y)*(L_UARM_Y+L_LARM_Y) +
                      (L_LARM_Z*L_LARM_Z));
            l2 = sqrt(L_FARM_Y*L_FARM_Y + L_FARM_Z*L_FARM_Z);
        } else {
            l1 = sqrt((R_UARM_Y+R_LARM_Y)*(R_UARM_Y+R_LARM_Y) +
                      (R_LARM_Z*R_LARM_Z));
            l2 = sqrt(R_FARM_Y*R_FARM_Y + R_FARM_Z*R_FARM_Z);
        }
        double xx = t1Wrist[3];
        double yy = t1Wrist[7];
        double zz = t1Wrist[11];
        
        double cosarg = (xx*xx + yy*yy + zz*zz - l1*l1 - l2*l2)/(2.0 * l1 * l2);
        
        if (fabs(cosarg - 1.0) < 1e-3) cosarg = 1.0;
        if (fabs(cosarg + 1.0) < 1e-3) cosarg = -1.0;
        
#if 0
        if (i == 112) {
            printf("A1: %.6f %g %g %g %g %g\n", cosarg, l1, l2, xx, yy, zz);
        }
#endif
        
        // this means the goal point is out of reach
        if (fabs(cosarg) > 1.0) {
#if 0
            if (i == 112) {
                printf("ELX: ARM OUT OF REACH\n"); 
            }
#endif
            
            // invalidate all solutions
            for (int j=0; j<4; j++) {
                solutions[i].valid[j] = 0;
            }
            continue;
        }
        
        // 0 & 2 are pos, 1 & 3 are neg
        solutions[i].angles[0][3] = acos(cosarg);
        solutions[i].angles[1][3] = -acos(cosarg);
        solutions[i].angles[2][3] = solutions[i].angles[0][3];
        solutions[i].angles[3][3] = solutions[i].angles[1][3];
        
        for (int j=0; j<4; j++) {
            solutions[i].valid[j] = 1;
        }
        
#if 0
        if (i == 112) {
            printf("ELX1: %g\n", solutions[i].angles[0][3]*R2D);
        }
#endif
        
        // now compute "false shoulder" joint
        double falseShoulder[2];
        double k1, k2;
        for (int j=0; j<2; j++) {
            k1 = l1 + l2 * cos(solutions[i].angles[j][3]);
            k2 = l2 * sin(solutions[i].angles[j][3]);
            
            falseShoulder[j] = atan2(yy, sqrt(zz*zz+xx*xx)) - atan2(k2, k1) - atan(L_LARM_Z/(L_UARM_Y+L_LARM_Y));
            
#if 0
            if (i == 112) {
                printf("BB: %g %g %g\n", k1, k2, falseShoulder[j]*R2D);
            }
#endif
        }
        
        // now compute the correct ELX
        double cFS, sFS;
        double a, b, w, z;
        for (int j=0; j<2; j++) {
            cFS = cos(falseShoulder[j]);
            sFS = sin(falseShoulder[j]);
            
            a = L_FARM_Y * cFS + L_LARM_Z * sFS;
            b = L_FARM_Y * sFS - L_LARM_Z * cFS;
            w = sqrt(zz*zz+xx*xx) - (L_UARM_Y+L_LARM_Y) * cFS + L_LARM_Z * sFS;
            z = yy - (L_UARM_Y+L_LARM_Y) * sFS - L_LARM_Z * cFS;
            
            solutions[i].angles[j][3] = atan2((a*z - b*w) , (a*w + b*z));
            solutions[i].angles[j+2][3] = solutions[i].angles[j][3];
            
            if (side == RIGHT) {
                solutions[i].angles[j][3] = -solutions[i].angles[j][3];
                solutions[i].angles[j+2][3] = -solutions[i].angles[j+2][3];
            }
            
#if 0
            if (i == 112) {
                printf("CC: %g %g %g %g = %g", a, b, w, z, solutions[i].angles[j][3]*R2D);
            }
#endif
        }
        
#if 0
        printf("RAW ELX: (%d) %g (%d) %g (%d) %g (%d) %g\n", 
               solutions[i].valid[0], solutions[i].angles[0][3]*R2D,
               solutions[i].valid[1], solutions[i].angles[1][3]*R2D,
               solutions[i].valid[2], solutions[i].angles[2][3]*R2D,
               solutions[i].valid[3], solutions[i].angles[3][3]*R2D);
#endif
        
        // Compute ELY using inverse sin
        // if ELX == 0, then ELY is undefined. Just set it to 0
        for (int j=0; j<2; j++) {
            
            if (fabs(solutions[i].angles[j][3]) < 1e-5) {
                solutions[i].angles[j][2] = 0.0;
                solutions[i].angles[j+2][2] = 0.0;
            } else {
                
                double sinarg;
                if (side == LEFT) {
                    sinarg = (xx / (sin(solutions[i].angles[j][3])*L_FARM_Y +
                                    cos(solutions[i].angles[j][3])*L_FARM_Z + 
                                    L_LARM_Z));
                } else {
                    sinarg = (xx / (sin(solutions[i].angles[j][3])*R_FARM_Y +
                                    cos(solutions[i].angles[j][3])*R_FARM_Z + 
                                    R_LARM_Z));
                }
#if 0
                printf("ELY: %g %g: %g\n", xx, solutions[i].angles[j][3], sinarg);
#endif

//                if (fabs(sinarg - 1.0) < 1e-1) sinarg = 1.0;
//                if (fabs(sinarg + 1.0) < 1e-1) sinarg = -1.0;
                
                if (fabs(sinarg) > 1.0) {
                    solutions[i].valid[j] = 0;
                    solutions[i].valid[j+2] = 0;
#if 0
                    if (i == 112) {
                        printf("INVALID SINARG %g", sinarg);
                    }
#endif
                } else {
                    // keep track of both inverse sin results
                    solutions[i].angles[j][2] = asin(sinarg);
                    solutions[i].angles[j+2][2] = M_PI - solutions[i].angles[j][2];
                }
            }
        }
        
#if 0
        printf("RAW ELY: (%d) %g (%d) %g (%d) %g (%d) %g\n", 
               solutions[i].valid[0], solutions[i].angles[0][2]*R2D,
               solutions[i].valid[1], solutions[i].angles[1][2]*R2D,
               solutions[i].valid[2], solutions[i].angles[2][2]*R2D,
               solutions[i].valid[3], solutions[i].angles[3][2]*R2D);
#endif
      
        // compute the shoulder joint
        for (int j=0; j<4; j++) {
            
            if (solutions[i].valid[j] == 1) {
                
                // Compute SHX by solving 2x2 matrix
                double aa, bb;
                if (side == LEFT) {
                    aa = (l1 + l2*cos(solutions[i].angles[j][3]) - 
                          L_FARM_Z*sin(solutions[i].angles[j][3]));
                    
                    bb = (l2*cos(solutions[i].angles[j][2])*
                          sin(solutions[i].angles[j][3]) +
                          
                          L_FARM_Z*cos(solutions[i].angles[j][2])*
                          cos(solutions[i].angles[j][3]) + 
                          
                          L_LARM_Z*cos(solutions[i].angles[j][2]));
                } else {
                    aa = (l1 + l2*cos(solutions[i].angles[j][3]) - 
                          R_FARM_Z*sin(solutions[i].angles[j][3]));
                    
                    bb = (l2*cos(solutions[i].angles[j][2])*
                          sin(solutions[i].angles[j][3]) +
                          
                          R_FARM_Z*cos(solutions[i].angles[j][2])*
                          cos(solutions[i].angles[j][3]) + 
                          
                          R_LARM_Z*cos(solutions[i].angles[j][2]));
                }
                
                solutions[i].angles[j][1] = atan((aa*zz - bb*yy)/(aa*yy + bb*zz));
            }
        }
        
#if 0
        printf("RAW MSX: (%d) %g (%d) %g (%d) %g (%d) %g\n", 
               solutions[i].valid[0], solutions[i].angles[0][1]*R2D,
               solutions[i].valid[1], solutions[i].angles[1][1]*R2D,
               solutions[i].valid[2], solutions[i].angles[2][1]*R2D,
               solutions[i].valid[3], solutions[i].angles[3][1]*R2D);
#endif
        
        // range checks 
        for (int j=0; j<4; j++) {
            
            if (side == LEFT) {
                if (solutions[i].angles[j][1] < L_MSX_MIN || 
                    solutions[i].angles[j][1] > L_MSX_MAX ||
                    solutions[i].angles[j][2] < L_ELY_MIN ||
                    solutions[i].angles[j][2] > L_ELY_MAX ||
                    solutions[i].angles[j][3] < L_ELX_MIN ||
                    solutions[i].angles[j][3] > L_ELX_MAX) {
                    solutions[i].valid[j] = 0;
                }
            } else {
                if (solutions[i].angles[j][1] < R_MSX_MIN || 
                    solutions[i].angles[j][1] > R_MSX_MAX ||
                    solutions[i].angles[j][2] < R_ELY_MIN ||
                    solutions[i].angles[j][2] > R_ELY_MAX ||
                    solutions[i].angles[j][3] < R_ELX_MIN ||
                    solutions[i].angles[j][3] > R_ELX_MAX) {
#if 0
                    printf("%d: MSX (%.2f) , ELY (%.2f), or ELX (%.2f) is out of range\n", j,
                           solutions[i].angles[j][1]*R2D,
                           solutions[i].angles[j][2]*R2D,
                           solutions[i].angles[j][3]*R2D);
#endif
                    solutions[i].valid[j] = 0;
                }
            }
        }
        
        // Step n: Using the found 3 angles, compute forward kinematics
        for (int j=0; j<4; j++) {
            if (solutions[i].valid[j] == 1) {
                
                // Get rid of those 3 angles and compute uwy and mwx                    
                double a[9];
                a[0] = torso[0];
                a[1] = torso[1];
                a[2] = torso[2];
                a[3] = solutions[i].angles[j][0];
                a[4] = solutions[i].angles[j][1];
                a[5] = solutions[i].angles[j][2];
                a[6] = solutions[i].angles[j][3];
                
#if 0
                if (i == 112) {
                    printf("USING: %.2f %.2f %.2f %.2f\n", 
                           solutions[i].angles[j][0]*R2D,
                           solutions[i].angles[j][1]*R2D,
                           solutions[i].angles[j][2]*R2D,
                           solutions[i].angles[j][3]*R2D);
                }
#endif
                
                fkToWrist(side, a, tSoFar);
                
                invertTransform(tSoFar, tSoFarInv);
                
                multiplyTransform(tSoFarInv, tIn, t4Wrist);
#if 0
                printf("%d: %d (%g)\n", i, j, solutions[i].angles[j][0]*R2D);
                printTransform("tSoFar", tSoFar);
                printTransform("tSoFarI", tSoFarInv);
                printTransform("t4Wrist", t4Wrist);
#endif
                // some simple calcs from the rotation matrix
                solutions[i].angles[j][4] = atan2(-t4Wrist[8], t4Wrist[0]);
                solutions[i].angles[j][5] = atan2(-t4Wrist[6], t4Wrist[5]);
            }
        }
        
#if 0
        printf("RAW UWY: (%d) %g (%d) %g (%d) %g (%d) %g\n", 
               solutions[i].valid[0], solutions[i].angles[0][4]*R2D,
               solutions[i].valid[1], solutions[i].angles[1][4]*R2D,
               solutions[i].valid[2], solutions[i].angles[2][4]*R2D,
               solutions[i].valid[3], solutions[i].angles[3][4]*R2D);
            
        printf("RAW MWX: (%d) %g (%d) %g (%d) %g (%d) %g\n", 
               solutions[i].valid[0], solutions[i].angles[0][5]*R2D,
               solutions[i].valid[1], solutions[i].angles[1][5]*R2D,
               solutions[i].valid[2], solutions[i].angles[2][5]*R2D,
               solutions[i].valid[3], solutions[i].angles[3][5]*R2D);
#endif
        
        // range checks 
        for (int j=0; j<4; j++) {
            if (solutions[i].valid[j] == 1) {
                if (side == LEFT) {
                    if (solutions[i].angles[j][4] < L_UWY_MIN || 
                        solutions[i].angles[j][4] > L_UWY_MAX ||
                        solutions[i].angles[j][5] < L_MWX_MIN ||
                        solutions[i].angles[j][5] > L_MWX_MAX) {
                        solutions[i].valid[j] = 0;
                    } 
                } else {
                    if (solutions[i].angles[j][4] < R_UWY_MIN || 
                        solutions[i].angles[j][4] > R_UWY_MAX ||
                        solutions[i].angles[j][5] < R_MWX_MIN ||
                        solutions[i].angles[j][5] > R_MWX_MAX) {
                        solutions[i].valid[j] = 0;
                    }
                }
            } 
        }
        
        // Step n: Use the forward kinematics to compute the 
        // translational error and save the smallest
        for (int j=0; j<4; j++) {
            if (solutions[i].valid[j]) {
                
                double a[9];
                double tTemp[12];
                a[0] = torso[0];
                a[1] = torso[1];
                a[2] = torso[2];
                a[3] = solutions[i].angles[j][0];
                a[4] = solutions[i].angles[j][1];
                a[5] = solutions[i].angles[j][2];
                a[6] = solutions[i].angles[j][3];
                a[7] = solutions[i].angles[j][4];
                a[8] = solutions[i].angles[j][5];
                
                fk(side, a, tTemp);
                
#if 0
                printf("Using solution: %.2f %.2f %.2f %.2f %.2f %.2f\n",
                       solutions[i].angles[j][0]*R2D,
                       solutions[i].angles[j][1]*R2D,
                       solutions[i].angles[j][2]*R2D,
                       solutions[i].angles[j][3]*R2D,
                       solutions[i].angles[j][4]*R2D,
                       solutions[i].angles[j][5]*R2D);
                printf("Ttemp for solution %d (%.2f %.2f %.2f)\n", i,
                       tIn[3], tIn[7], tIn[11]);
                printTransform(NULL, tTemp);
#endif        
                solutions[i].error[j] = 
                    sqrt((tTemp[3]-tIn[3])*(tTemp[3]-tIn[3]) +
                         (tTemp[7]-tIn[7])*(tTemp[7]-tIn[7]) +
                         (tTemp[11]-tIn[11])*(tTemp[11]-tIn[11]));
                
                // save the global minumum
                if (solutions[i].error[j] < solutionError) {
#if 0
                    printf("ITER %d saving Error %g < %g\n", i,
                           solutions[i].error[j], solutionError);
#endif
                    
                    solutionError = solutions[i].error[j];
                    solutionIndex = i;
                }
            }  
        }  
    }

    int anyValid = 0;
    for (int i=0; i<NUM_STEPS; i++) {
        if (solutions[i].valid[0] || solutions[i].valid[1] || solutions[i].valid[2] || solutions[i].valid[3]) {
            anyValid = 1;
            break;
        }
    }
    if (anyValid == 0) {
#if 0
        printf("THERE ARE NO VALID SOLUTIONS\n");
#endif
        return 0;
    }

    *numSolsP = 0;
    
    // check if the errors of any solution is below the error threshold. 
    for (int j=0; j<4; j++) {
        if ((solutions[solutionIndex].valid[j] == 1) &&
            (solutions[solutionIndex].error[j] < MIN_ERROR)) {
            
            for (int i=0; i<6; i++) {
                aOut[*numSolsP][i] = solutions[solutionIndex].angles[j][i];
            }
            error[*numSolsP] = solutions[solutionIndex].error[j];
            
            validSolution = 1;
            (*numSolsP)++;
        }
    }
    
    return validSolution;
}


int ikWrist(int side, double tIn[12], double torso[9], int *numSolsP, double aOut[4][6], double error[4])
{
    double t16[12];
    double t1Wrist[12];
    double t45[12];
//    double t4Wrist[12];
    double t56[12];
    double t6wrist[12];
    double tTorso[12];
//    double tSoFar[12];
//    double tSoFarInv[12];
    int    solutionIndex = 0;
    int    validSolution = 0;
    double solutionError = 1e10;
    SOLUTION_T solutions[NUM_STEPS];
//    int loopCtr = 1;
    double shoulderSearchStartAngle = -112.0 * D2R;
    double shoulderSearchResolution = 0.1 * D2R;

#if 0
    printTransform("tIn", tIn);
#endif

        
    // Step 1: Search across the upper shoulder y joint 
    for (int i=0; i<NUM_STEPS; i++) {
        for (int j=0; j<4; j++) {
            solutions[i].angles[j][0] = (i * shoulderSearchResolution + shoulderSearchStartAngle);
        }
            
#if 0
        printf("\nITERATION %d  RAW USY: %g\n", i,
               solutions[i].angles[0][0]*R2D);
#endif
        
        // Step 2: Remove the torso angles and upper shoulder y transform and offset 
        double tTemp1[12];
        double tTemp2[12];
        double tTorsoInv[12];
        double t54[12];
        double t65[12];
        
#if 0
        if (i == 0) {
            printf("TORSO: %.2f %.2f %.2f\n", 
                   torso[0]*R2D,
                   torso[1]*R2D,
                   torso[2]*R2D);
        }
#endif
        fkTorso(side, torso, tTorso);
        
        invertTransform(tTorso, tTorsoInv);
        
        multiplyTransform(tTorsoInv, tIn, tTemp1);
        
#if 0
        if (i == 112) {
            printTransform("tTemp1", tTemp1);
        }
#endif
        
        // clav origin to clav (upper shoulder y joint - really rotates about z)
        t45[0] = cos(solutions[i].angles[0][0]);
        t45[1] = -sin(solutions[i].angles[0][0]);
        t45[2] = 0;
        t45[3] = 0;
        t45[4] = sin(solutions[i].angles[0][0]);
        t45[5] = cos(solutions[i].angles[0][0]);
        t45[6] = 0;
        t45[7] = 0;
        t45[8] = 0;
        t45[9] = 0;
        t45[10] = 1;
        t45[11] = 0;
        
        invertTransform(t45, t54);
        
        multiplyTransform(t54, tTemp1, tTemp2);
        
        // clav to scap (translation only)
        t56[0] = 1;
        t56[1] = 0;
        t56[2] = 0;
        t56[3] = 0;
        t56[4] = 0;
        t56[5] = 1;
        t56[6] = 0;
        t56[8] = 0;
        t56[9] = 0;
        t56[10] = 1;
        if (side == LEFT) {
            t56[7] = L_SCAP_Y;
            t56[11] = L_SCAP_Z;
        } else {
            t56[7] = R_SCAP_Y;
            t56[11] = R_SCAP_Z;
        }
        
        invertTransform(t56, t65);
        
        multiplyTransform(t65, tTemp2, t16);
        
#if 0
        if (i == 112) {
            printTransform("t16", t16);
        }
#endif

        // CHANGE CHANGE CHANGE
        // Step 3: Get rid of the L_HAND_Y link length (DOES NOTHING FOR IK WRIST)
        t6wrist[0] = 1;
        t6wrist[1] = 0;
        t6wrist[2] = 0;
        t6wrist[3] = 0;
        t6wrist[4] = 0;
        t6wrist[5] = 1;
        t6wrist[6] = 0;
        t6wrist[7] = 0;
        t6wrist[8] = 0;
        t6wrist[9] = 0;
        t6wrist[10] = 1;
        t6wrist[11] = 0;
        
        multiplyTransform(t16, t6wrist, t1Wrist);
        
#if 0
        if (i == 112) {
            printTransform("t1Wrist", t1Wrist);
        }
#endif
        
        // Compute ELX for "false arm" using law of cosines
        double l1, l2;
        if (side == LEFT) {
            l1 = sqrt((L_UARM_Y+L_LARM_Y)*(L_UARM_Y+L_LARM_Y) +
                      (L_LARM_Z*L_LARM_Z));
            l2 = sqrt(L_FARM_Y*L_FARM_Y + L_FARM_Z*L_FARM_Z);
        } else {
            l1 = sqrt((R_UARM_Y+R_LARM_Y)*(R_UARM_Y+R_LARM_Y) +
                      (R_LARM_Z*R_LARM_Z));
            l2 = sqrt(R_FARM_Y*R_FARM_Y + R_FARM_Z*R_FARM_Z);
        }
        double xx = t1Wrist[3];
        double yy = t1Wrist[7];
        double zz = t1Wrist[11];
        
        double cosarg = (xx*xx + yy*yy + zz*zz - l1*l1 - l2*l2)/(2.0 * l1 * l2);
        
        if (fabs(cosarg - 1.0) < 1e-3) cosarg = 1.0;
        if (fabs(cosarg + 1.0) < 1e-3) cosarg = -1.0;
        
#if 0
        if (i == 112) {
            printf("A1: %.6f %g %g %g %g %g\n", cosarg, l1, l2, xx, yy, zz);
        }
#endif
        
        // this means the goal point is out of reach
        if (fabs(cosarg) > 1.0) {
#if 0
            if (i == 112) {
                printf("ELX: ARM OUT OF REACH\n"); 
            }
#endif
            
            // invalidate all solutions
            for (int j=0; j<4; j++) {
                solutions[i].valid[j] = 0;
            }
            continue;
        }
        
        // 0 & 2 are pos, 1 & 3 are neg
        solutions[i].angles[0][3] = acos(cosarg);
        solutions[i].angles[1][3] = -acos(cosarg);
        solutions[i].angles[2][3] = solutions[i].angles[0][3];
        solutions[i].angles[3][3] = solutions[i].angles[1][3];
        
        for (int j=0; j<4; j++) {
            solutions[i].valid[j] = 1;
        }
        
#if 0
        if (i == 112) {
            printf("ELX1: %g\n", solutions[i].angles[0][3]*R2D);
        }
#endif
        
        // now compute "false shoulder" joint
        double falseShoulder[2];
        double k1, k2;
        for (int j=0; j<2; j++) {
            k1 = l1 + l2 * cos(solutions[i].angles[j][3]);
            k2 = l2 * sin(solutions[i].angles[j][3]);
            
            falseShoulder[j] = atan2(yy, sqrt(zz*zz+xx*xx)) - atan2(k2, k1) - atan(L_LARM_Z/(L_UARM_Y+L_LARM_Y));
            
#if 0
            if (i == 112) {
                printf("BB: %g %g %g\n", k1, k2, falseShoulder[j]*R2D);
            }
#endif
        }
        
        // now compute the correct ELX
        double cFS, sFS;
        double a, b, w, z;
        for (int j=0; j<2; j++) {
            cFS = cos(falseShoulder[j]);
            sFS = sin(falseShoulder[j]);
            
            a = L_FARM_Y * cFS + L_LARM_Z * sFS;
            b = L_FARM_Y * sFS - L_LARM_Z * cFS;
            w = sqrt(zz*zz+xx*xx) - (L_UARM_Y+L_LARM_Y) * cFS + L_LARM_Z * sFS;
            z = yy - (L_UARM_Y+L_LARM_Y) * sFS - L_LARM_Z * cFS;
            
            solutions[i].angles[j][3] = atan2((a*z - b*w) , (a*w + b*z));
            solutions[i].angles[j+2][3] = solutions[i].angles[j][3];
            
            if (side == RIGHT) {
                solutions[i].angles[j][3] = -solutions[i].angles[j][3];
                solutions[i].angles[j+2][3] = -solutions[i].angles[j+2][3];
            }
            
#if 0
            if (i == 112) {
                printf("CC: %g %g %g %g = %g", a, b, w, z, solutions[i].angles[j][3]*R2D);
            }
#endif
        }
        
#if 0
        printf("RAW ELX: (%d) %g (%d) %g (%d) %g (%d) %g\n", 
               solutions[i].valid[0], solutions[i].angles[0][3]*R2D,
               solutions[i].valid[1], solutions[i].angles[1][3]*R2D,
               solutions[i].valid[2], solutions[i].angles[2][3]*R2D,
               solutions[i].valid[3], solutions[i].angles[3][3]*R2D);
#endif
        
        // Compute ELY using inverse sin
        // if ELX == 0, then ELY is undefined. Just set it to 0
        for (int j=0; j<2; j++) {
            
            if (fabs(solutions[i].angles[j][3]) < 1e-5) {
                solutions[i].angles[j][2] = 0.0;
                solutions[i].angles[j+2][2] = 0.0;
            } else {
                
                double sinarg;
                if (side == LEFT) {
                    sinarg = (xx / (sin(solutions[i].angles[j][3])*L_FARM_Y +
                                    cos(solutions[i].angles[j][3])*L_FARM_Z + 
                                    L_LARM_Z));
                } else {
                    sinarg = (xx / (sin(solutions[i].angles[j][3])*R_FARM_Y +
                                    cos(solutions[i].angles[j][3])*R_FARM_Z + 
                                    R_LARM_Z));
                }
#if 0
                printf("ELY: %g %g: %g\n", xx, solutions[i].angles[j][3], sinarg);
#endif

//                if (fabs(sinarg - 1.0) < 1e-1) sinarg = 1.0;
//                if (fabs(sinarg + 1.0) < 1e-1) sinarg = -1.0;
                
                if (fabs(sinarg) > 1.0) {
                    solutions[i].valid[j] = 0;
                    solutions[i].valid[j+2] = 0;
#if 0
                    if (i == 112) {
                        printf("INVALID SINARG %g", sinarg);
                    }
#endif
                } else {
                    // keep track of both inverse sin results
                    solutions[i].angles[j][2] = asin(sinarg);
                    solutions[i].angles[j+2][2] = M_PI - solutions[i].angles[j][2];
                }
            }
        }
        
#if 0
        printf("RAW ELY: (%d) %g (%d) %g (%d) %g (%d) %g\n", 
               solutions[i].valid[0], solutions[i].angles[0][2]*R2D,
               solutions[i].valid[1], solutions[i].angles[1][2]*R2D,
               solutions[i].valid[2], solutions[i].angles[2][2]*R2D,
               solutions[i].valid[3], solutions[i].angles[3][2]*R2D);
#endif
      
        // compute the shoulder joint
        for (int j=0; j<4; j++) {
            
            if (solutions[i].valid[j] == 1) {
                
                // Compute SHX by solving 2x2 matrix
                double aa, bb;
                if (side == LEFT) {
                    aa = (l1 + l2*cos(solutions[i].angles[j][3]) - 
                          L_FARM_Z*sin(solutions[i].angles[j][3]));
                    
                    bb = (l2*cos(solutions[i].angles[j][2])*
                          sin(solutions[i].angles[j][3]) +
                          
                          L_FARM_Z*cos(solutions[i].angles[j][2])*
                          cos(solutions[i].angles[j][3]) + 
                          
                          L_LARM_Z*cos(solutions[i].angles[j][2]));
                } else {
                    aa = (l1 + l2*cos(solutions[i].angles[j][3]) - 
                          R_FARM_Z*sin(solutions[i].angles[j][3]));
                    
                    bb = (l2*cos(solutions[i].angles[j][2])*
                          sin(solutions[i].angles[j][3]) +
                          
                          R_FARM_Z*cos(solutions[i].angles[j][2])*
                          cos(solutions[i].angles[j][3]) + 
                          
                          R_LARM_Z*cos(solutions[i].angles[j][2]));
                }
                
                solutions[i].angles[j][1] = atan((aa*zz - bb*yy)/(aa*yy + bb*zz));
            }
        }
        
#if 0
        printf("RAW MSX: (%d) %g (%d) %g (%d) %g (%d) %g\n", 
               solutions[i].valid[0], solutions[i].angles[0][1]*R2D,
               solutions[i].valid[1], solutions[i].angles[1][1]*R2D,
               solutions[i].valid[2], solutions[i].angles[2][1]*R2D,
               solutions[i].valid[3], solutions[i].angles[3][1]*R2D);
#endif
        
        // range checks 
        for (int j=0; j<4; j++) {
            
            if (side == LEFT) {
                if (solutions[i].angles[j][1] < L_MSX_MIN || 
                    solutions[i].angles[j][1] > L_MSX_MAX ||
                    solutions[i].angles[j][2] < L_ELY_MIN ||
                    solutions[i].angles[j][2] > L_ELY_MAX ||
                    solutions[i].angles[j][3] < L_ELX_MIN ||
                    solutions[i].angles[j][3] > L_ELX_MAX) {
                    solutions[i].valid[j] = 0;
                }
            } else {
                if (solutions[i].angles[j][1] < R_MSX_MIN || 
                    solutions[i].angles[j][1] > R_MSX_MAX ||
                    solutions[i].angles[j][2] < R_ELY_MIN ||
                    solutions[i].angles[j][2] > R_ELY_MAX ||
                    solutions[i].angles[j][3] < R_ELX_MIN ||
                    solutions[i].angles[j][3] > R_ELX_MAX) {
#if 0
                    printf("%d: MSX (%.2f) , ELY (%.2f), or ELX (%.2f) is out of range\n", j,
                           solutions[i].angles[j][1]*R2D,
                           solutions[i].angles[j][2]*R2D,
                           solutions[i].angles[j][3]*R2D);
#endif
                    solutions[i].valid[j] = 0;
                }
            }
        }

        // CHANGE CHANGE CHANGE
        for (int j=0; j<4; j++) {
            solutions[i].angles[j][4] = 0;
            solutions[i].angles[j][5] = 0;
        }
#if 0
        // Step n: Using the found 3 angles, compute forward kinematics
        for (int j=0; j<4; j++) {
            if (solutions[i].valid[j] == 1) {
                
                // Get rid of those 3 angles and compute uwy and mwx                    
                double a[9];
                a[0] = torso[0];
                a[1] = torso[1];
                a[2] = torso[2];
                a[3] = solutions[i].angles[j][0];
                a[4] = solutions[i].angles[j][1];
                a[5] = solutions[i].angles[j][2];
                a[6] = solutions[i].angles[j][3];
                
#if 0
                if (i == 112) {
                    printf("USING: %.2f %.2f %.2f %.2f\n", 
                           solutions[i].angles[j][0]*R2D,
                           solutions[i].angles[j][1]*R2D,
                           solutions[i].angles[j][2]*R2D,
                           solutions[i].angles[j][3]*R2D);
                }
#endif
                
                fkToWrist(side, a, tSoFar);
                
                invertTransform(tSoFar, tSoFarInv);
                
                multiplyTransform(tSoFarInv, tIn, t4Wrist);
#if 0
                printf("%d: %d (%g)\n", i, j, solutions[i].angles[j][0]*R2D);
                printTransform("tSoFar", tSoFar);
                printTransform("tSoFarI", tSoFarInv);
                printTransform("t4Wrist", t4Wrist);
#endif
                // some simple calcs from the rotation matrix
                solutions[i].angles[j][4] = atan2(-t4Wrist[8], t4Wrist[0]);
                solutions[i].angles[j][5] = atan2(-t4Wrist[6], t4Wrist[5]);
            }
        }
        
#if 0
        printf("RAW UWY: (%d) %g (%d) %g (%d) %g (%d) %g\n", 
               solutions[i].valid[0], solutions[i].angles[0][4]*R2D,
               solutions[i].valid[1], solutions[i].angles[1][4]*R2D,
               solutions[i].valid[2], solutions[i].angles[2][4]*R2D,
               solutions[i].valid[3], solutions[i].angles[3][4]*R2D);
            
        printf("RAW MWX: (%d) %g (%d) %g (%d) %g (%d) %g\n", 
               solutions[i].valid[0], solutions[i].angles[0][5]*R2D,
               solutions[i].valid[1], solutions[i].angles[1][5]*R2D,
               solutions[i].valid[2], solutions[i].angles[2][5]*R2D,
               solutions[i].valid[3], solutions[i].angles[3][5]*R2D);
#endif
        
        // range checks 
        for (int j=0; j<4; j++) {
            if (solutions[i].valid[j] == 1) {
                if (side == LEFT) {
                    if (solutions[i].angles[j][4] < L_UWY_MIN || 
                        solutions[i].angles[j][4] > L_UWY_MAX ||
                        solutions[i].angles[j][5] < L_MWX_MIN ||
                        solutions[i].angles[j][5] > L_MWX_MAX) {
                        solutions[i].valid[j] = 0;
                    } 
                } else {
                    if (solutions[i].angles[j][4] < R_UWY_MIN || 
                        solutions[i].angles[j][4] > R_UWY_MAX ||
                        solutions[i].angles[j][5] < R_MWX_MIN ||
                        solutions[i].angles[j][5] > R_MWX_MAX) {
                        solutions[i].valid[j] = 0;
                    }
                }
            } 
        }
#endif
        
        // Step n: Use the forward kinematics to compute the 
        // translational error and save the smallest
        for (int j=0; j<4; j++) {
            if (solutions[i].valid[j]) {
                
                double a[9];
                double tTemp[12];
                a[0] = torso[0];
                a[1] = torso[1];
                a[2] = torso[2];
                a[3] = solutions[i].angles[j][0];
                a[4] = solutions[i].angles[j][1];
                a[5] = solutions[i].angles[j][2];
                a[6] = solutions[i].angles[j][3];
                a[7] = solutions[i].angles[j][4];
                a[8] = solutions[i].angles[j][5];
                
                // CHANGE CHANGE CHANGE
                fkToWrist(side, a, tTemp);
                
#if 0
                printf("Using solution: %.2f %.2f %.2f %.2f %.2f %.2f\n",
                       solutions[i].angles[j][0]*R2D,
                       solutions[i].angles[j][1]*R2D,
                       solutions[i].angles[j][2]*R2D,
                       solutions[i].angles[j][3]*R2D,
                       solutions[i].angles[j][4]*R2D,
                       solutions[i].angles[j][5]*R2D);
                printf("Ttemp for solution %d (%.2f %.2f %.2f)\n", i,
                       tIn[3], tIn[7], tIn[11]);
                printTransform(NULL, tTemp);
#endif        
                solutions[i].error[j] = 
                    sqrt((tTemp[3]-tIn[3])*(tTemp[3]-tIn[3]) +
                         (tTemp[7]-tIn[7])*(tTemp[7]-tIn[7]) +
                         (tTemp[11]-tIn[11])*(tTemp[11]-tIn[11]));
                
                // save the global minumum
                if (solutions[i].error[j] < solutionError) {
#if 0
                    printf("ITER %d saving Error %g < %g\n", i,
                           solutions[i].error[j], solutionError);
#endif
                    
                    solutionError = solutions[i].error[j];
                    solutionIndex = i;
                }
            }  
        }  
    }

    int anyValid = 0;
    for (int i=0; i<NUM_STEPS; i++) {
        if (solutions[i].valid[0] || solutions[i].valid[1] || solutions[i].valid[2] || solutions[i].valid[3]) {
            anyValid = 1;
            break;
        }
    }
    if (anyValid == 0) {
#if 0
        printf("THERE ARE NO VALID SOLUTIONS\n");
#endif
        return 0;
    }

    *numSolsP = 0;
    
    // check if the errors of any solution is below the error threshold. 
    for (int j=0; j<4; j++) {
        if ((solutions[solutionIndex].valid[j] == 1) &&
            (solutions[solutionIndex].error[j] < MIN_ERROR)) {
            
            for (int i=0; i<6; i++) {
                aOut[*numSolsP][i] = solutions[solutionIndex].angles[j][i];
            }
            error[*numSolsP] = solutions[solutionIndex].error[j];
            
            validSolution = 1;
            (*numSolsP)++;
        }
    }
    
    return validSolution;
}

