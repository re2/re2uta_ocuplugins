/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include <re2uta_ocuPlugins/StopCmdPlugin.h>
#include <std_msgs/Empty.h>

#include <QHBoxLayout>

StopCmdPlugin::StopCmdPlugin()
{
    // Create the layouts
    QHBoxLayout *hBoxLayout = new QHBoxLayout(this);

    m_stopButton = new QPushButton("STOP ALL", this);
    hBoxLayout->addWidget(m_stopButton);
    connect(m_stopButton, SIGNAL(released()), this, SLOT(stopButton()));

    m_node.param<std::string>("/re2uta/StopCmd_rosTopicReqVal", m_rosTopic, "/ui/stop_cmd");
    m_stopPub = m_node.advertise<std_msgs::Empty>( m_rosTopic, 1, this);

    show();
}

void StopCmdPlugin::stopButton()
{
    std_msgs::Empty msg;

    m_stopPub.publish(msg);
}

/**
 * Required for rviz plug-in
 */
void StopCmdPlugin::saveToConfig( const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config )
{
}

/**
 * Required for rviz plug-in
 */
void StopCmdPlugin::loadFromConfig( const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config )
{
}

/**
 * Required for rviz plug-in
 */
void StopCmdPlugin::onInitialize()
{
}

// Tell pluginlib about this class.  Every class which should be
// loadable by pluginlib::ClassLoader must have these two lines
// compiled in its .cpp file, outside of any namespace scope.
#include <pluginlib/class_list_macros.h>
PLUGINLIB_DECLARE_CLASS( re2uta_ocuPlugins, StopCmd, StopCmdPlugin, rviz::Panel )

