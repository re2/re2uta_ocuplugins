/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include <ros/ros.h>
#include <re2uta_ocuPlugins/JointCmdPlugin.h>
#include <re2/eigen/eigen_util.h>
#include <QHBoxLayout>
#include <QVBoxLayout>

#include <tf/transform_listener.h>
#include <tf_conversions/tf_eigen.h>
#include <eigen_conversions/eigen_msg.h>

using namespace re2uta;

JointCmdPlugin::JointCmdPlugin() : Super()
{
    // joint labels
    m_label[0] = new QLabel("LBZ");
    m_label[1] = new QLabel("MBY");
    m_label[2]= new QLabel("UBX");

    m_label[3] = new QLabel("NECK");

    m_label[4] = new QLabel("L UHZ");
    m_label[5] = new QLabel("L MHX");
    m_label[6]= new QLabel("L LHY");
    m_label[7]= new QLabel("L KNY");
    m_label[8]= new QLabel("L UAY");
    m_label[9]= new QLabel("L LAX");

    m_label[10] = new QLabel("R UHZ");
    m_label[11] = new QLabel("R MHX");
    m_label[12]= new QLabel("R LHY");
    m_label[13]= new QLabel("R KNY");
    m_label[14]= new QLabel("R UAY");
    m_label[15]= new QLabel("R LAX");

    m_label[16] = new QLabel("L USY");
    m_label[17] = new QLabel("L MSX");
    m_label[18]= new QLabel("L ELY");
    m_label[19]= new QLabel("L ELX");
    m_label[20]= new QLabel("L UWY");
    m_label[21]= new QLabel("L MWX");

    m_label[22] = new QLabel("R USY");
    m_label[23] = new QLabel("R MSX");
    m_label[24]= new QLabel("R ELY");
    m_label[25]= new QLabel("R ELX");
    m_label[26]= new QLabel("R UWY");
    m_label[27]= new QLabel("R MWX");

    // command / actual labels
    m_label[28] = new QLabel("CMD");
    m_label[29] = new QLabel("ACT");
    m_label[30] = new QLabel("CMD");
    m_label[31] = new QLabel("ACT");
    m_label[32] = new QLabel("CMD");
    m_label[33] = new QLabel("ACT");
    m_label[34] = new QLabel("CMD");
    m_label[35] = new QLabel("ACT");
    m_label[36] = new QLabel("CMD");
    m_label[37] = new QLabel("ACT");

    // blank labels
    for (int i=38; i<43; i++) {
        m_label[i] = new QLabel("");
    }

    for (int i=0; i<56; i++) {
        m_term[i] = new QDoubleSpinBox();
        (m_term[i])->setSingleStep(1);
        (m_term[i])->setDecimals(1);
    }

#if 0
    // set ranges
    for (int i=0; i<56; i++) {
        (m_term[i])->setRange(-180, 180);
    }
#endif

    // HARD CODED - BAD BAD BAD
    (m_term[0])->setRange(-0.610865*R2D, 0.610865*R2D);
    (m_term[1])->setRange(-0.610865*R2D, 0.610865*R2D);
    (m_term[2])->setRange(-1.2*R2D, 1.28*R2D);
    (m_term[3])->setRange(-1.2*R2D, 1.28*R2D);
    (m_term[4])->setRange(-0.790809*R2D, 0.790809*R2D);
    (m_term[5])->setRange(-0.790809*R2D, 0.790809*R2D);
    (m_term[6])->setRange(-0.610865*R2D, 1.134464*R2D);
    (m_term[7])->setRange(-0.610865*R2D, 1.134464*R2D);

    (m_term[8])->setRange(-0.32*R2D, 1.14*R2D);
    (m_term[9])->setRange(-0.32*R2D, 1.14*R2D);
    (m_term[10])->setRange(-0.47*R2D, 0.495*R2D);
    (m_term[11])->setRange(-0.47*R2D, 0.495*R2D);
    (m_term[12])->setRange(-1.75*R2D, 0.524*R2D);
    (m_term[13])->setRange(-1.75*R2D, 0.524*R2D);
    (m_term[14])->setRange(0*R2D, 2.45*R2D);
    (m_term[15])->setRange(0*R2D, 2.45*R2D);
    (m_term[16])->setRange(-0.698*R2D, 0.698*R2D);
    (m_term[17])->setRange(-0.698*R2D, 0.698*R2D);
    (m_term[18])->setRange(-0.436*R2D, 0.436*R2D);
    (m_term[19])->setRange(-0.436*R2D, 0.436*R2D);

    (m_term[20])->setRange(-1.14*R2D, 0.32*R2D);
    (m_term[21])->setRange(-1.14*R2D, 0.32*R2D);
    (m_term[22])->setRange(-0.495*R2D, 0.47*R2D);
    (m_term[23])->setRange(-0.495*R2D, 0.47*R2D);
    (m_term[24])->setRange(-1.745*R2D, 0.524*R2D);
    (m_term[25])->setRange(-1.745*R2D, 0.524*R2D);
    (m_term[26])->setRange(0*R2D, 2.45*R2D);
    (m_term[27])->setRange(0*R2D, 2.45*R2D);
    (m_term[28])->setRange(-0.698*R2D, 0.698*R2D);
    (m_term[29])->setRange(-0.698*R2D, 0.698*R2D);
    (m_term[30])->setRange(-0.436*R2D, 0.436*R2D);
    (m_term[31])->setRange(-0.436*R2D, 0.436*R2D);

    (m_term[32])->setRange(-1.9635*R2D, 1.9635*R2D);
    (m_term[33])->setRange(-1.9635*R2D, 1.9635*R2D);
    (m_term[34])->setRange(-1.39626*R2D, 1.74533*R2D);
    (m_term[35])->setRange(-1.39626*R2D, 1.74533*R2D);
    (m_term[36])->setRange(0*R2D, 3.14159*R2D);
    (m_term[37])->setRange(0*R2D, 3.14159*R2D);
    (m_term[38])->setRange(0*R2D, 2.35619*R2D);
    (m_term[39])->setRange(0*R2D, 2.35619*R2D);
    (m_term[40])->setRange(-1.571*R2D, 1.571*R2D);
    (m_term[41])->setRange(-1.571*R2D, 1.571*R2D);
    (m_term[42])->setRange(-0.436*R2D, 1.571*R2D);
    (m_term[43])->setRange(-0.436*R2D, 1.571*R2D);

    (m_term[44])->setRange(-1.9635*R2D, 1.9635*R2D);
    (m_term[45])->setRange(-1.9635*R2D, 1.9635*R2D);
    (m_term[46])->setRange(-1.74533*R2D, 1.39626*R2D);
    (m_term[47])->setRange(-1.74533*R2D, 1.39626*R2D);
    (m_term[48])->setRange(0*R2D, 3.14159*R2D);
    (m_term[49])->setRange(0*R2D, 3.14159*R2D);
    (m_term[50])->setRange(-2.35619*R2D, 0*R2D);
    (m_term[51])->setRange(-2.35619*R2D, 0*R2D);
    (m_term[52])->setRange(-1.571*R2D, 1.571*R2D);
    (m_term[53])->setRange(-1.571*R2D, 1.571*R2D);
    (m_term[54])->setRange(-1.571*R2D, 0.436*R2D);
    (m_term[55])->setRange(-1.571*R2D, 0.436*R2D);

    // slider spin boxes for weights
    for (int i=0; i<6; i++) {
      m_weightSlider[i] = new SliderAndSpinBox();
      (m_weightSlider[i])->setRange(0, 50);
    }
    m_weightSlider[0]->setSliderVal(30);
    m_weightSlider[1]->setSliderVal(30);
    m_weightSlider[2]->setSliderVal(30);
    m_weightSlider[3]->setSliderVal(5);
    m_weightSlider[4]->setSliderVal(5);
    m_weightSlider[5]->setSliderVal(5);

    // create the buttons
    m_button[0] = new QPushButton("UPDATE", this);
    m_button[1] = new QPushButton("FILL ACT", this);
    m_button[2] = new QPushButton("FILL IM", this);
    m_button[3] = new QPushButton("", this);
    m_button[4] = new QPushButton("SEND FRAME", this);
    m_button[5] = new QPushButton("SEND JT CMD", this);
    m_button[6] = new QPushButton("", this);
    m_button[7] = new QPushButton("", this);
    m_button[8] = new QPushButton("", this);

//    connect(m_button[0], SIGNAL(released()), this, SLOT(updateButtonCB()));
    connect(m_button[0], SIGNAL(released()), this, SLOT(updateJointState()));
    connect(m_button[1], SIGNAL(released()), this, SLOT(resetJointStateActual()));
    connect(m_button[2], SIGNAL(released()), this, SLOT(resetJointStateIM()));

    connect(m_button[4], SIGNAL(released()), this, SLOT(sendFrameCmd()));
    connect(m_button[5], SIGNAL(released()), this, SLOT(sendJointCmd()));

    int numHBox = 8;
    QHBoxLayout *hbox[numHBox];
    QVBoxLayout *vbox = new QVBoxLayout(this);

    for (int i=0; i<numHBox; i++) {
        hbox[i] = new QHBoxLayout();
    }

    // TITLE BOX
    (hbox[0])->addWidget(m_label[38]);
    (hbox[0])->addWidget(m_label[28]);
    (hbox[0])->addWidget(m_label[29]);
    (hbox[0])->addWidget(m_label[39]);
    (hbox[0])->addWidget(m_label[30]);
    (hbox[0])->addWidget(m_label[31]);
    (hbox[0])->addWidget(m_label[40]);
    (hbox[0])->addWidget(m_label[32]);
    (hbox[0])->addWidget(m_label[33]);
    (hbox[0])->addWidget(m_label[41]);
    (hbox[0])->addWidget(m_label[34]);
    (hbox[0])->addWidget(m_label[35]);
    (hbox[0])->addWidget(m_label[42]);
    (hbox[0])->addWidget(m_label[36]);
    (hbox[0])->addWidget(m_label[37]);

    (hbox[1])->addWidget(m_label[0]);
    (hbox[1])->addWidget(m_term[0]);
    (hbox[1])->addWidget(m_term[1]);
    (hbox[1])->addWidget(m_label[4]);
    (hbox[1])->addWidget(m_term[8]);
    (hbox[1])->addWidget(m_term[9]);
    (hbox[1])->addWidget(m_label[10]);
    (hbox[1])->addWidget(m_term[20]);
    (hbox[1])->addWidget(m_term[21]);
    (hbox[1])->addWidget(m_label[16]);
    (hbox[1])->addWidget(m_term[32]);
    (hbox[1])->addWidget(m_term[33]);
    (hbox[1])->addWidget(m_label[22]);
    (hbox[1])->addWidget(m_term[44]);
    (hbox[1])->addWidget(m_term[45]);

    (hbox[2])->addWidget(m_label[1]);
    (hbox[2])->addWidget(m_term[2]);
    (hbox[2])->addWidget(m_term[3]);
    (hbox[2])->addWidget(m_label[5]);
    (hbox[2])->addWidget(m_term[10]);
    (hbox[2])->addWidget(m_term[11]);
    (hbox[2])->addWidget(m_label[11]);
    (hbox[2])->addWidget(m_term[22]);
    (hbox[2])->addWidget(m_term[23]);
    (hbox[2])->addWidget(m_label[17]);
    (hbox[2])->addWidget(m_term[34]);
    (hbox[2])->addWidget(m_term[35]);
    (hbox[2])->addWidget(m_label[23]);
    (hbox[2])->addWidget(m_term[46]);
    (hbox[2])->addWidget(m_term[47]);

    (hbox[3])->addWidget(m_label[2]);
    (hbox[3])->addWidget(m_term[4]);
    (hbox[3])->addWidget(m_term[5]);
    (hbox[3])->addWidget(m_label[6]);
    (hbox[3])->addWidget(m_term[12]);
    (hbox[3])->addWidget(m_term[13]);
    (hbox[3])->addWidget(m_label[12]);
    (hbox[3])->addWidget(m_term[24]);
    (hbox[3])->addWidget(m_term[25]);
    (hbox[3])->addWidget(m_label[18]);
    (hbox[3])->addWidget(m_term[36]);
    (hbox[3])->addWidget(m_term[37]);
    (hbox[3])->addWidget(m_label[24]);
    (hbox[3])->addWidget(m_term[48]);
    (hbox[3])->addWidget(m_term[49]);

    (hbox[4])->addWidget(m_label[3]);
    (hbox[4])->addWidget(m_term[6]);
    (hbox[4])->addWidget(m_term[7]);
    (hbox[4])->addWidget(m_label[7]);
    (hbox[4])->addWidget(m_term[14]);
    (hbox[4])->addWidget(m_term[15]);
    (hbox[4])->addWidget(m_label[13]);
    (hbox[4])->addWidget(m_term[26]);
    (hbox[4])->addWidget(m_term[27]);
    (hbox[4])->addWidget(m_label[19]);
    (hbox[4])->addWidget(m_term[38]);
    (hbox[4])->addWidget(m_term[39]);
    (hbox[4])->addWidget(m_label[25]);
    (hbox[4])->addWidget(m_term[50]);
    (hbox[4])->addWidget(m_term[51]);

    (hbox[5])->addWidget(m_button[0]);
    (hbox[5])->addWidget(m_button[1]);
    (hbox[5])->addWidget(m_button[2]);
    (hbox[5])->addWidget(m_label[8]);
    (hbox[5])->addWidget(m_term[16]);
    (hbox[5])->addWidget(m_term[17]);
    (hbox[5])->addWidget(m_label[14]);
    (hbox[5])->addWidget(m_term[28]);
    (hbox[5])->addWidget(m_term[29]);
    (hbox[5])->addWidget(m_label[20]);
    (hbox[5])->addWidget(m_term[40]);
    (hbox[5])->addWidget(m_term[41]);
    (hbox[5])->addWidget(m_label[26]);
    (hbox[5])->addWidget(m_term[52]);
    (hbox[5])->addWidget(m_term[53]);

    (hbox[6])->addWidget(m_button[3]);
    (hbox[6])->addWidget(m_button[4]);
    (hbox[6])->addWidget(m_button[5]);
    (hbox[6])->addWidget(m_label[9]);
    (hbox[6])->addWidget(m_term[18]);
    (hbox[6])->addWidget(m_term[19]);
    (hbox[6])->addWidget(m_label[15]);
    (hbox[6])->addWidget(m_term[30]);
    (hbox[6])->addWidget(m_term[31]);
    (hbox[6])->addWidget(m_label[21]);
    (hbox[6])->addWidget(m_term[42]);
    (hbox[6])->addWidget(m_term[43]);
    (hbox[6])->addWidget(m_label[27]);
    (hbox[6])->addWidget(m_term[54]);
    (hbox[6])->addWidget(m_term[55]);

    (hbox[7])->addWidget(m_button[6]);
    (hbox[7])->addWidget(m_button[7]);
    (hbox[7])->addWidget(m_button[8]);
    (hbox[7])->addWidget(m_weightSlider[0]);
    (hbox[7])->addWidget(m_weightSlider[1]);
    (hbox[7])->addWidget(m_weightSlider[2]);
    (hbox[7])->addWidget(m_weightSlider[3]);
    (hbox[7])->addWidget(m_weightSlider[4]);
    (hbox[7])->addWidget(m_weightSlider[5]);

    for (int i=0; i<numHBox; i++) {
        vbox->addLayout(hbox[i]);
    }

    show();

    m_frameCmdPub = m_node.advertise<re2uta_atlasCommander::FrameCommandStamped>("/ui/frame_cmd", 1, this);

    m_jointCmdPub = m_node.advertise< re2uta_inetComms::JointCommand >( "/ui/joint_cmds", 1, this);

    m_reqJointStatePub = m_node.advertise< std_msgs::Empty >("/ui/jointStateHandlerReq", 1, this);
    m_jointStateSub = m_node.subscribe("/ui/jointStateHandlerData", 1, &JointCmdPlugin::jointStateCallback, this);

    // pose coming from rviz (interactive markers)
    m_markerSub = m_node.subscribe("/planning_visualizer_controls/feedback", 1, &JointCmdPlugin::imMarkerCallback, this);

    // joint states coming from rviz (interactive markers)
    m_imSub = m_node.subscribe("/joint_states", 1, &JointCmdPlugin::imCallback, this);
    m_imJoints.position.resize(107);

    m_jointCmdMsg.valid.resize(28);
    m_jointCmdMsg.cmd.position.resize(28);

    for (int i=0; i<28; i++) {
        m_jointCmdMsg.valid[i] = 0;
        m_jointCmdMsg.cmd.position[i] = 0;
    }

    m_currentAngles.position.resize(28);


    // set up stuff to publish the state to the ghost
    std::string simpleUrdfString;

    while (!m_node.hasParam("/robot_description")) {
        ROS_WARN_ONCE("[JointCmdPlugin] Waiting to receive robot description...");
        ros::spinOnce();
    }

    m_node.getParam("/robot_description", simpleUrdfString);
    m_urdfModel.initString(simpleUrdfString);
    m_atlasDebugPublisher.reset(new re2uta::AtlasDebugPublisher(m_urdfModel, "/ghost"));
    
    // Note: number seems to be Hz
    m_ghostPublishTimer = m_node.createTimer( 20.0, &JointCmdPlugin::handleGhostPublishTimer, this );
//    std::string tfPrefix;
//        m_atlasDebugPublisher->setWorld("/control/pelvis");
//        m_atlasDebugPublisher->setBase("pelvis");

//    m_DEBUGTimer = m_node.createTimer( 0.0001, &JointCmdPlugin::DEBUGTimer, this );
}

JointCmdPlugin::~JointCmdPlugin()
{
}

//void JointCmdPlugin::DEBUGTimer( const ros::TimerEvent & event )
//{
//    updateJointState();
//}

void JointCmdPlugin::handleGhostPublishTimer( const ros::TimerEvent & event )
{
#if 0
    // send to ghost
    ROS_INFO("LEFT LEG: %g %g %g %g %g %g",
             m_currentAngles.position[4]*R2D,
             m_currentAngles.position[5]*R2D,
             m_currentAngles.position[6]*R2D,
             m_currentAngles.position[7]*R2D,
             m_currentAngles.position[8]*R2D,
             m_currentAngles.position[9]*R2D);
#endif

    m_atlasDebugPublisher->publishCmdOrderedAngles(re2::toEigen(m_currentAngles.position).cast<double>());
}

// CALLBACKS
void JointCmdPlugin::imCallback(const sensor_msgs::JointState::ConstPtr& msg)
{
  m_imJoints.position = msg->position;
}


void JointCmdPlugin::imMarkerCallback(const visualization_msgs::InteractiveMarkerFeedback::ConstPtr& msg)
{
  m_imMarkers.header = msg->header;
  m_imMarkers.client_id = msg->client_id;
  m_imMarkers.marker_name = msg->marker_name;
  m_imMarkers.control_name = msg->control_name;
  m_imMarkers.event_type = msg->event_type;
  m_imMarkers.pose = msg->pose;
  m_imMarkers.menu_entry_id = msg->menu_entry_id;
  m_imMarkers.mouse_point = msg->mouse_point;
  m_imMarkers.mouse_point_valid = msg->mouse_point_valid;
}
    
void JointCmdPlugin::jointStateCallback(const sensor_msgs::JointState::ConstPtr& msg)
{
    m_currentAngles.position = msg->position;

    // show the current angles
    for (int i=0; i<28; i++) {
        (m_term[2*i+1])->setValue(m_currentAngles.position[i]*R2D);
    }
}

void JointCmdPlugin::resetJointStateActual()
{
  for (int i=0; i<28; i++) {
    (m_term[2*i])->setValue((m_term[2*i+1])->value());
  }
}

void JointCmdPlugin::resetJointStateIM()
{
    // convert from rviz joint states to atlas joint state indices
    (m_term[0])->setValue(m_imJoints.position[1]*R2D);
    (m_term[2])->setValue(m_imJoints.position[2]*R2D);
    (m_term[4])->setValue(m_imJoints.position[3]*R2D);
    (m_term[6])->setValue(m_imJoints.position[43]*R2D);

    (m_term[8])->setValue(m_imJoints.position[95]*R2D);
    (m_term[10])->setValue(m_imJoints.position[96]*R2D);
    (m_term[12])->setValue(m_imJoints.position[97]*R2D);
    (m_term[14])->setValue(m_imJoints.position[98]*R2D);
    (m_term[16])->setValue(m_imJoints.position[99]*R2D);
    (m_term[18])->setValue(m_imJoints.position[100]*R2D);

    (m_term[20])->setValue(m_imJoints.position[101]*R2D);
    (m_term[22])->setValue(m_imJoints.position[102]*R2D);
    (m_term[24])->setValue(m_imJoints.position[103]*R2D);
    (m_term[26])->setValue(m_imJoints.position[104]*R2D);
    (m_term[28])->setValue(m_imJoints.position[105]*R2D);
    (m_term[30])->setValue(m_imJoints.position[106]*R2D);

    (m_term[32])->setValue(m_imJoints.position[4]*R2D);
    (m_term[34])->setValue(m_imJoints.position[5]*R2D);
    (m_term[36])->setValue(m_imJoints.position[6]*R2D);
    (m_term[38])->setValue(m_imJoints.position[7]*R2D);
    (m_term[40])->setValue(m_imJoints.position[8]*R2D);
    (m_term[42])->setValue(m_imJoints.position[9]*R2D);

    (m_term[44])->setValue(m_imJoints.position[55]*R2D);
    (m_term[46])->setValue(m_imJoints.position[56]*R2D);
    (m_term[48])->setValue(m_imJoints.position[57]*R2D);
    (m_term[50])->setValue(m_imJoints.position[58]*R2D);
    (m_term[52])->setValue(m_imJoints.position[59]*R2D);
    (m_term[54])->setValue(m_imJoints.position[60]*R2D);
}

//void JointCmdPlugin::updateButtonCB()
//{
//    ROS_INFO("UPDATE BUTTON PRESSED");
//    ros::Duration dur(3);
//    m_DEBUGTimer.setPeriod(dur);
//
//    updateJointState();
//}

void JointCmdPlugin::updateJointState()
{
//    ROS_INFO("UPDATING JOINT STATE");
    std_msgs::Empty msg;

    m_reqJointStatePub.publish(msg);
}

void JointCmdPlugin::sendFrameCmd()
{
  re2uta_atlasCommander::FrameCommandStamped msg;

  ROS_INFO("IN SEND FRAME CMD");
  msg.header.frame_id = "l_foot";

  // NEEDS TESTED
  msg.goalPose.header.frame_id = "odom";

  if (m_imMarkers.marker_name == "left_arm" ||
      m_imMarkers.marker_name == "torso_left_arm") {
    msg.target_frame_id = "l_hand";
  } else if (m_imMarkers.marker_name == "right_arm" ||
         m_imMarkers.marker_name == "torso_right_arm") {
    msg.target_frame_id = "r_hand";
  } else if (m_imMarkers.marker_name == "torso_head") {
    msg.target_frame_id = "head";
  } else {
     ROS_INFO("[JOINT CMD PLUGIN] ERROR - MARKER FRAME IS %s", m_imMarkers.marker_name.c_str());
     return;
  }

#if 0
  else if (m_imMarkers.marker_name == "left_leg") {
    msg.target_frame_id = "l_foot";
  } else if (m_imMarkers.marker_name == "right_leg") {
    msg.target_frame_id = "r_foot";
  }
#endif
  
  msg.weights.linear.x = (m_weightSlider[0])->getSpinBoxVal() * 0.01;
  msg.weights.linear.y = (m_weightSlider[1])->getSpinBoxVal() * 0.01;
  msg.weights.linear.z = (m_weightSlider[2])->getSpinBoxVal() * 0.01;
  msg.weights.angular.x = (m_weightSlider[3])->getSpinBoxVal() * 0.01;
  msg.weights.angular.y = (m_weightSlider[4])->getSpinBoxVal() * 0.01;
  msg.weights.angular.z = (m_weightSlider[5])->getSpinBoxVal() * 0.01;

  // convert the imMarkers pose, which is in the pelvis frame, to the odom frame
  tf::StampedTransform pelvisToOdomTF;
  m_tfListener.lookupTransform("/odom", "/pelvis", ros::Time(0), pelvisToOdomTF);

  Eigen::Affine3d pelvisToOdomEigen;
  tf::TransformTFToEigen(pelvisToOdomTF, pelvisToOdomEigen);

  Eigen::Affine3d tipPoseInPelvisEigen;
  tf::poseMsgToEigen(m_imMarkers.pose, tipPoseInPelvisEigen);

  Eigen::Affine3d tipPoseInOdomEigen;
  tipPoseInOdomEigen = pelvisToOdomEigen * tipPoseInPelvisEigen;

  tf::poseEigenToMsg(tipPoseInOdomEigen, msg.goalPose.pose);

  
  ROS_INFO("SENDING FRAME COMMAND MSG");
  ROS_INFO("BASE FRAME: %s", msg.header.frame_id.c_str());
  ROS_INFO("TIP FRAME: %s", msg.target_frame_id.c_str());
  ROS_INFO("GOAL FRAME: %s", msg.goalPose.header.frame_id.c_str());
  ROS_INFO("WEIGHTS: %g %g %g %g %g %g",
       msg.weights.linear.x,
       msg.weights.linear.y,
       msg.weights.linear.z,
       msg.weights.angular.x,
       msg.weights.angular.y,
       msg.weights.angular.z);
  ROS_INFO("POSE: %g %g %g %g %g %g %g",
       msg.goalPose.pose.position.x,
       msg.goalPose.pose.position.y,
       msg.goalPose.pose.position.z,
       msg.goalPose.pose.orientation.x,
       msg.goalPose.pose.orientation.y,
       msg.goalPose.pose.orientation.z,
       msg.goalPose.pose.orientation.w);
  
  m_frameCmdPub.publish(msg);
}

void JointCmdPlugin::sendJointCmd()
{
    // check to see if any commands have changed from the last send
    for (int i=0; i<28; i++) {
//        ROS_INFO("%d: %g %g", i, (m_term[2*i])->value(), m_jointCmdMsg.cmd.position[i]*R2D);
        if (fabs( (m_term[2*i])->value()*D2R - m_jointCmdMsg.cmd.position[i] ) > CMD_CHANGE) {
//            ROS_INFO("VALID CHANGE");
            m_jointCmdMsg.cmd.position[i] = (m_term[2*i])->value()*D2R;
            m_jointCmdMsg.valid[i] = 1;
        } else {
//            ROS_INFO("NO CHANGE");
            m_jointCmdMsg.valid[i] = 0;
        }
    }

    m_jointCmdPub.publish(m_jointCmdMsg);
}

/**
 * Required for rviz plug-in
 */
void JointCmdPlugin::saveToConfig( const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config )
{
}
/**
 * Required for rviz plug-in
 */
void JointCmdPlugin::loadFromConfig( const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config )
{
}

/**
 * Required for rviz plug-in
 */
void JointCmdPlugin::onInitialize()
{
}

// Tell pluginlib about this class.  Every class which should be
// loadable by pluginlib::ClassLoader must have these two lines
// compiled in its .cpp file, outside of any namespace scope.
#include <pluginlib/class_list_macros.h>
PLUGINLIB_DECLARE_CLASS( re2uta_ocuPlugins, Joints, JointCmdPlugin, rviz::Panel )

