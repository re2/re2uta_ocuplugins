/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include <re2uta_ocuPlugins/PointCloudHandlerPlugin.h>

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>

#include <visualization_msgs/Marker.h>

#include <re2uta_inetComms/ReqPointCloudData.h>

static const std::string COMM_LVL_SMALLEST_STR("Smallest");
static const std::string COMM_LVL_SMALL_STR(   "Small");
static const std::string COMM_LVL_MEDIUM_STR(  "Medium");
static const std::string COMM_LVL_LARGE_STR(   "Large");
static const std::string COMM_LVL_LARGEST_STR( "Largest");

// TODO: This is duplicated in DataHandler... combine?
static const int UNKNOWN = 0;

static const int COMM_LVL_SMALLEST = 1;
static const int COMM_LVL_SMALL    = 2;
static const int COMM_LVL_MEDIUM   = 3;
static const int COMM_LVL_LARGE    = 4;
static const int COMM_LVL_LARGEST  = 5;

PointCloudHandlerPlugin::PointCloudHandlerPlugin()
{
    // Create the layouts
    QVBoxLayout *finalVbox  = new QVBoxLayout(this);
    QHBoxLayout *hBoxLayout = new QHBoxLayout();

    QHBoxLayout *minMaxHboxLayout = new QHBoxLayout();
    QVBoxLayout *xVbox = new QVBoxLayout();
    QVBoxLayout *yVbox = new QVBoxLayout();
    QVBoxLayout *zVbox = new QVBoxLayout();

    QLabel *xLabel = new QLabel("X Min/Max");
    QLabel *yLabel = new QLabel("Y Min/Max");
    QLabel *zLabel = new QLabel("Z Min/Max");

    // Create and setup the combo boxes
    m_commLvlCombo = new QComboBox();
    m_commLvlCombo->addItem(tr(COMM_LVL_SMALLEST_STR.c_str()));
    m_commLvlCombo->addItem(tr(COMM_LVL_SMALL_STR.c_str()));
    m_commLvlCombo->addItem(tr(COMM_LVL_MEDIUM_STR.c_str()));
    m_commLvlCombo->addItem(tr(COMM_LVL_LARGE_STR.c_str()));
    m_commLvlCombo->addItem(tr(COMM_LVL_LARGEST_STR.c_str()));

    // and the spin boxes
    m_xMinBox = new QDoubleSpinBox();
    m_xMaxBox = new QDoubleSpinBox();
    m_yMinBox = new QDoubleSpinBox();
    m_yMaxBox = new QDoubleSpinBox();
    m_zMinBox = new QDoubleSpinBox();
    m_zMaxBox = new QDoubleSpinBox();

    double stepSize =   0.1;
    double min      = -30.0;
    double max      =  30.0;

    m_xMinBox->setSingleStep(stepSize);
    m_xMaxBox->setSingleStep(stepSize);
    m_yMinBox->setSingleStep(stepSize);
    m_yMaxBox->setSingleStep(stepSize);
    m_zMinBox->setSingleStep(stepSize);
    m_zMaxBox->setSingleStep(stepSize);

    m_xMinBox->setMinimum(min);
    m_xMaxBox->setMinimum(min);
    m_yMinBox->setMinimum(min);
    m_yMaxBox->setMinimum(min);
    m_zMinBox->setMinimum(min);
    m_zMaxBox->setMinimum(min);

    m_xMinBox->setMaximum(max);
    m_xMaxBox->setMaximum(max);
    m_yMinBox->setMaximum(max);
    m_yMaxBox->setMaximum(max);
    m_zMinBox->setMaximum(max);
    m_zMaxBox->setMaximum(max);

    // Set default values if none are saved in config file
    m_xMinBox->setValue(min);
    m_xMaxBox->setValue(max);
    m_yMinBox->setValue(min);
    m_yMaxBox->setValue(max);
    m_zMinBox->setValue(min);
    m_zMaxBox->setValue(max);

    // and the button
    m_requestButton = new QPushButton("Request Point Cloud", this);

    // Add the widgets to the layout
    xVbox->addWidget(xLabel);
    xVbox->addWidget(m_xMinBox);
    xVbox->addWidget(m_xMaxBox);
    yVbox->addWidget(yLabel);
    yVbox->addWidget(m_yMinBox);
    yVbox->addWidget(m_yMaxBox);
    zVbox->addWidget(zLabel);
    zVbox->addWidget(m_zMinBox);
    zVbox->addWidget(m_zMaxBox);

    minMaxHboxLayout->addLayout(xVbox);
    minMaxHboxLayout->addLayout(yVbox);
    minMaxHboxLayout->addLayout(zVbox);

    hBoxLayout->addWidget(m_commLvlCombo);
    hBoxLayout->addWidget(m_requestButton);

    finalVbox->addLayout(minMaxHboxLayout);
    finalVbox->addLayout(hBoxLayout);

    // Connect button signal to appropriate slot
    connect(m_requestButton, SIGNAL(released()), this, SLOT(handleButton()));

    // Create a timer for publishing the most recent data
    m_visPub = m_node.advertise<visualization_msgs::Marker>( "visualization_marker", 0 );
    m_timer = m_node.createTimer(ros::Duration( 0.3), &PointCloudHandlerPlugin::update, this);

    // Grab the handler id for PointCloudHandler
    int tempHandlerId;
    m_node.param<int>("/re2uta/PointCloudHandler_id", tempHandlerId, 0);
    m_handlerId = tempHandlerId;

    // Publishers
    m_node.param<std::string>("/re2uta/PointCloudHandler_rosTopicReqVal", m_rosTopic, "/ui/pointCloudHandlerReq");
    m_requestPub = m_node.advertise< re2uta_inetComms::ReqPointCloudData >( m_rosTopic, 1, this);

    show();
}

/**
 * The request button has been pressed. Send a request message to the data handler.
 */
void PointCloudHandlerPlugin::handleButton()
{
    re2uta_inetComms::ReqPointCloudData msg;

    msg.handlerId = m_handlerId;

    std::string currentVal = m_commLvlCombo->currentText().toStdString();
    if(currentVal == COMM_LVL_SMALLEST_STR)
    {
        msg.commLvl = COMM_LVL_SMALLEST;
    }
    else if(currentVal == COMM_LVL_SMALL_STR)
    {
        msg.commLvl = COMM_LVL_SMALL;
    }
    else if(currentVal == COMM_LVL_MEDIUM_STR)
    {
        msg.commLvl = COMM_LVL_MEDIUM;
    }
    else if(currentVal == COMM_LVL_LARGE_STR)
    {
        msg.commLvl = COMM_LVL_LARGE;
    }
    else if(currentVal == COMM_LVL_LARGEST_STR)
    {
        msg.commLvl = COMM_LVL_LARGEST;
    }
    else
    {
        ROS_WARN("[PointCloudHandlerPlugin] Unknown commLvl: %s",currentVal.c_str());
        msg.commLvl = UNKNOWN;
    }

    // Min/Max values
    msg.xMin = m_xMinBox->value();
    msg.xMax = m_xMaxBox->value();
    msg.yMin = m_yMinBox->value();
    msg.yMax = m_yMaxBox->value();
    msg.zMin = m_zMinBox->value();
    msg.zMax = m_zMaxBox->value();

    m_requestPub.publish(msg);
}

void PointCloudHandlerPlugin::update( const ros::TimerEvent& e)
{
    visualization_msgs::Marker marker;
    float boxWidth  = (m_yMaxBox->value()-m_yMinBox->value());
    float boxHeight = (m_zMaxBox->value()-m_zMinBox->value());
    float boxDepth  = (m_xMaxBox->value()-m_xMinBox->value());

    marker.header.frame_id = "/pelvis";
    marker.header.stamp = ros::Time();
//  marker.ns = "my_namespace";
    marker.id = 0;
    marker.type = visualization_msgs::Marker::CUBE;
    marker.action = visualization_msgs::Marker::ADD;

    marker.pose.position.x = m_xMinBox->value() + (boxDepth/2.0);
    marker.pose.position.y = m_yMinBox->value() + (boxWidth/2.0);
    marker.pose.position.z = m_zMinBox->value() + (boxHeight/2.0);

    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;

    marker.scale.x = boxDepth;
    marker.scale.y = boxWidth;
    marker.scale.z = boxHeight;

    marker.color.a = 0.25;
    marker.color.r = 0.0;
    marker.color.g = 1.0;
    marker.color.b = 0.0;

//  //only if using a MESH_RESOURCE marker type:
//  marker.mesh_resource = "package://pr2_description/meshes/base_v0/base.dae";

    m_visPub.publish( marker );
}

/**
 * Required for rviz plug-in
 */
void PointCloudHandlerPlugin::saveToConfig( const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config )
{
    float configVal;

    config->set( "PointCloudHandler_commLvl", m_commLvlCombo->currentText().toStdString() );

    configVal = static_cast<float>(m_xMinBox->value());
    config->set( "PointCloudHandler_xMin", configVal );

    configVal = static_cast<float>(m_xMaxBox->value());
    config->set( "PointCloudHandler_xMax", configVal );

    configVal = static_cast<float>(m_yMinBox->value());
    config->set( "PointCloudHandler_yMin", configVal );

    configVal = static_cast<float>(m_yMaxBox->value());
    config->set( "PointCloudHandler_yMax", configVal );

    configVal = static_cast<float>(m_zMinBox->value());
    config->set( "PointCloudHandler_zMin", configVal );

    configVal = static_cast<float>(m_zMaxBox->value());
    config->set( "PointCloudHandler_zMax", configVal );

}

/**
 * Required for rviz plug-in
 */
void PointCloudHandlerPlugin::loadFromConfig( const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config )
{
    std::string loadCommLvl;
    int index;
    float configVal;

    config->get( "PointCloudHandler_commLvl", &loadCommLvl, "DEFAULT VALUE" );

    index = m_commLvlCombo->findText(loadCommLvl.c_str());
    if ( index != -1 ) { // -1 for not found
        m_commLvlCombo->setCurrentIndex(index);
    }

    config->get( "PointCloudHandler_xMin", &configVal, 0.0 );
    m_xMinBox->setValue(static_cast<double>(configVal));

    config->get( "PointCloudHandler_xMax", &configVal, 0.0 );
    m_xMaxBox->setValue(static_cast<double>(configVal));

    config->get( "PointCloudHandler_yMin", &configVal, 0.0 );
    m_yMinBox->setValue(static_cast<double>(configVal));

    config->get( "PointCloudHandler_yMax", &configVal, 0.0 );
    m_yMaxBox->setValue(static_cast<double>(configVal));

    config->get( "PointCloudHandler_zMin", &configVal, 0.0 );
    m_zMinBox->setValue(static_cast<double>(configVal));

    config->get( "PointCloudHandler_zMax", &configVal, 0.0 );
    m_zMaxBox->setValue(static_cast<double>(configVal));

}

/**
 * Required for rviz plug-in
 */
void PointCloudHandlerPlugin::onInitialize()
{
}

// Tell pluginlib about this class.  Every class which should be
// loadable by pluginlib::ClassLoader must have these two lines
// compiled in its .cpp file, outside of any namespace scope.
#include <pluginlib/class_list_macros.h>
PLUGINLIB_DECLARE_CLASS( re2uta_ocuPlugins, PointCloudHandler, PointCloudHandlerPlugin, rviz::Panel )

