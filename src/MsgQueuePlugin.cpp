/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include <re2uta_ocuPlugins/MsgQueuePlugin.h>

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QString>

#include <sstream>
#include <vector>

MsgQueuePlugin::MsgQueuePlugin()
{
    QVBoxLayout *vbox = new QVBoxLayout();
    vbox->setSpacing(10);

    QHBoxLayout *hbox = new QHBoxLayout(this);

    m_listWidget = new QListWidget();
//    lw->addItem("The Omen");
//    lw->addItem("The Exorcist");
//    lw->addItem("Notes on a scandal");
//    lw->addItem("Fargo");
//    lw->addItem("Capote");

    m_runQueueButton     = new QPushButton("Run Queue",  this);
    m_removeButton       = new QPushButton("Remove",     this);
    m_removeAllButton    = new QPushButton("Remove All", this);
    m_requestQueueButton = new QPushButton("Refresh",    this);

    vbox->setSpacing(3);
    vbox->addStretch(1);
    vbox->addWidget(m_runQueueButton);
    vbox->addWidget(m_removeButton);
    vbox->addWidget(m_removeAllButton);
    vbox->addWidget(m_requestQueueButton);
    vbox->addStretch(1);

    hbox->addWidget(m_listWidget);
    hbox->addSpacing(15);
    hbox->addLayout(vbox);

    setLayout(hbox);

    connect(m_runQueueButton,     SIGNAL(released()), this, SLOT(executeQueue())   );
    connect(m_removeButton,       SIGNAL(released()), this, SLOT(removeItem())     );
    connect(m_removeAllButton,    SIGNAL(released()), this, SLOT(removeAllItems()) );
    connect(m_requestQueueButton, SIGNAL(released()), this, SLOT(requestQueue())   );

    m_queueActionPub = m_node.advertise< re2uta_inetComms::QueueInfo >( "/ui/msgQueueAction", 1, this);
    m_queueSub = m_node.subscribe("/ui/msgQueue", 1, &MsgQueuePlugin::updateQueue, this);

    show();
}

void MsgQueuePlugin::executeQueue()
{
    ROS_INFO("[MsgQueuePlugin] executeQueue() called");

    re2uta_inetComms::QueueInfo msg;
    msg.msgType = EXECUTE_MSG_QUEUE;

    m_queueActionPub.publish(msg);
}

void MsgQueuePlugin::removeItem()
{
    ROS_INFO("[MsgQueuePlugin] removeItem() called");

    re2uta_inetComms::QueueInfo msg;
    std::stringstream converter;
    int index = m_listWidget->currentRow();

    msg.msgType = REMOVE_FROM_MSG_QUEUE;

    converter << index;
    msg.queueContents.push_back(converter.str());

    m_queueActionPub.publish(msg);

//  QListWidgetItem *item = lw->currentItem();
//
//  if (item) {
//    int r = lw->row(item);
//    lw->takeItem(r);
//    delete item;
//  }
}

void MsgQueuePlugin::removeAllItems()
{
    ROS_INFO("[MsgQueuePlugin] clearItems() called");

    re2uta_inetComms::QueueInfo msg;
    msg.msgType = CLEAR_MSG_QUEUE;

    m_queueActionPub.publish(msg);
}

void MsgQueuePlugin::requestQueue()
{
    ROS_INFO("[MsgQueuePlugin] requestQueue() called");
    re2uta_inetComms::QueueInfo msg;
    msg.msgType = REQ_MSG_QUEUE;

    m_queueActionPub.publish(msg);
}

void MsgQueuePlugin::updateQueue(const re2uta_inetComms::QueueInfoConstPtr& msg)
{
    ROS_INFO("[MsgQueuePlugin] Queue info msg received");

    std::vector<std::string> theQueue;

    theQueue = msg->queueContents;

    if(m_listWidget->count() != 0)
    {
        m_listWidget->clear();
    }

    for(int i=0; i<theQueue.size(); ++i)
    {
        QString item = QString::fromStdString(theQueue.at(i));
        m_listWidget->addItem(item);
    }

    //  QString item = QInputDialog::getText(this, "Item",
    //         "Enter new item");
    //  item = item.simplified();
    //  if (!item.isEmpty()) {
    //    lw->addItem(item);
    //    int r = lw->count() - 1;
    //    lw->setCurrentRow(r);
    //  }
}


/**
 * Required for rviz plug-in
 */
void MsgQueuePlugin::saveToConfig( const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config )
{
}

/**
 * Required for rviz plug-in
 */
void MsgQueuePlugin::loadFromConfig( const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config )
{
}

/**
 * Required for rviz plug-in
 */
void MsgQueuePlugin::onInitialize()
{
}


// Tell pluginlib about this class.  Every class which should be
// loadable by pluginlib::ClassLoader must have these two lines
// compiled in its .cpp file, outside of any namespace scope.
#include <pluginlib/class_list_macros.h>
PLUGINLIB_DECLARE_CLASS( re2uta_ocuPlugins, MsgQueue, MsgQueuePlugin, rviz::Panel )

