/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include <re2uta_ocuPlugins/ImgHandlerPlugin.h>

#include <QHBoxLayout>
#include <re2uta_inetComms/ReqImgData.h>

static const std::string CAM_HEAD_STR(  "Head");
static const std::string CAM_L_HAND_STR("L Hand");
static const std::string CAM_R_HAND_STR("R Hand");

static const std::string COMM_LVL_SMALLEST_STR("Smallest");
static const std::string COMM_LVL_SMALL_STR(   "Small");
static const std::string COMM_LVL_MEDIUM_STR(  "Medium");
static const std::string COMM_LVL_LARGE_STR(   "Large");
static const std::string COMM_LVL_LARGEST_STR( "Largest");

// TODO: This is duplicated in DataHandler... combine?
static const int UNKNOWN = 0;

static const int CAM_HEAD   = 1;
static const int CAM_L_HAND = 2;
static const int CAM_R_HAND = 3;

static const int COMM_LVL_SMALLEST = 1;
static const int COMM_LVL_SMALL    = 2;
static const int COMM_LVL_MEDIUM   = 3;
static const int COMM_LVL_LARGE    = 4;
static const int COMM_LVL_LARGEST  = 5;

ImgHandlerPlugin::ImgHandlerPlugin()
{
    // Create the layouts
    QHBoxLayout *hBoxLayout = new QHBoxLayout(this);

    // Create and setup the combo boxes
    m_cameraCombo = new QComboBox();
    m_cameraCombo->addItem(tr(CAM_HEAD_STR.c_str()));
    m_cameraCombo->addItem(tr(CAM_L_HAND_STR.c_str()));
    m_cameraCombo->addItem(tr(CAM_R_HAND_STR.c_str()));

    m_commLvlCombo = new QComboBox();
    m_commLvlCombo->addItem(tr(COMM_LVL_SMALLEST_STR.c_str()));
    m_commLvlCombo->addItem(tr(COMM_LVL_SMALL_STR.c_str()));
    m_commLvlCombo->addItem(tr(COMM_LVL_MEDIUM_STR.c_str()));
    m_commLvlCombo->addItem(tr(COMM_LVL_LARGE_STR.c_str()));
    m_commLvlCombo->addItem(tr(COMM_LVL_LARGEST_STR.c_str()));

    // and the button
    m_requestButton = new QPushButton("Request Img", this);

    // Add the widgets to the layout
    hBoxLayout->addWidget(m_cameraCombo);
    hBoxLayout->addWidget(m_commLvlCombo);
    hBoxLayout->addWidget(m_requestButton);

    // Connect button signal to appropriate slot
    connect(m_requestButton, SIGNAL(released()), this, SLOT(handleButton()));

    // Create a timer for publishing the most recent img data
    m_timer = m_node.createTimer(ros::Duration( 0.05), &ImgHandlerPlugin::update, this);

    // Grab the handler id for ImgHandler
    int tempHandlerId;
    m_node.param<int>("/re2uta/ImgHandler_id", tempHandlerId, 0);
    m_handlerId = tempHandlerId;

    // Publishers
    m_node.param<std::string>("/re2uta/ImgHandler_rosTopicReqVal", m_rosTopic, "/ui/imgHandlerReq");
    m_requestPub = m_node.advertise< re2uta_inetComms::ReqImgData >( m_rosTopic, 1, this);

    show();
}

/**
 * The request button has been pressed. Send a request message to the data handler.
 */
void ImgHandlerPlugin::handleButton()
{
//    std_msgs::Char msg;
//    msg.data = m_handlerId;

    re2uta_inetComms::ReqImgData msg;

    msg.handlerId = m_handlerId;

    std::string currentCamVal = m_cameraCombo->currentText().toStdString();
    if(currentCamVal == CAM_HEAD_STR)
    {
        msg.camera = CAM_HEAD;
    }
    else if(currentCamVal == CAM_L_HAND_STR)
    {
        msg.camera = CAM_L_HAND;
    }
    else if(currentCamVal == CAM_R_HAND_STR)
    {
        msg.camera = CAM_R_HAND;
    }
    else
    {
        ROS_WARN("[ImgHandlerPlugin] Unknown camera: %s",currentCamVal.c_str());
        msg.camera = UNKNOWN;
    }

    std::string currentVal = m_commLvlCombo->currentText().toStdString();
    if(currentVal == COMM_LVL_SMALLEST_STR)
    {
        msg.commLvl = COMM_LVL_SMALLEST;
    }
    else if(currentVal == COMM_LVL_SMALL_STR)
    {
        msg.commLvl = COMM_LVL_SMALL;
    }
    else if(currentVal == COMM_LVL_MEDIUM_STR)
    {
        msg.commLvl = COMM_LVL_MEDIUM;
    }
    else if(currentVal == COMM_LVL_LARGE_STR)
    {
        msg.commLvl = COMM_LVL_LARGE;
    }
    else if(currentVal == COMM_LVL_LARGEST_STR)
    {
        msg.commLvl = COMM_LVL_LARGEST;
    }
    else
    {
        ROS_WARN("[ImgHandlerPlugin] Unknown commLvl: %s",currentVal.c_str());
        msg.commLvl = UNKNOWN;
    }

    m_requestPub.publish(msg);
}

void ImgHandlerPlugin::update( const ros::TimerEvent& e)
{
}

/**
 * Required for rviz plug-in
 */
void ImgHandlerPlugin::saveToConfig( const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config )
{
    config->set( "ImgHandler_commLvl", m_commLvlCombo->currentText().toStdString() );
    config->set( "ImgHandler_camera",  m_cameraCombo->currentText().toStdString() );
}

/**
 * Required for rviz plug-in
 */
void ImgHandlerPlugin::loadFromConfig( const std::string& key_prefix, const boost::shared_ptr<rviz::Config>& config )
{
    std::string loadCommLvl;
    std::string loadCam;
    int index;

    config->get( "ImgHandler_commLvl", &loadCommLvl, "DEFAULT VALUE" );
    config->get( "ImgHandler_camera", &loadCam,      "DEFAULT VALUE" );

    index = m_commLvlCombo->findText(loadCommLvl.c_str());
    if ( index != -1 ) { // -1 for not found
        m_commLvlCombo->setCurrentIndex(index);
    }

    index = m_cameraCombo->findText(loadCam.c_str());
    if ( index != -1 ) { // -1 for not found
        m_cameraCombo->setCurrentIndex(index);
    }
}

/**
 * Required for rviz plug-in
 */
void ImgHandlerPlugin::onInitialize()
{
}

// Tell pluginlib about this class.  Every class which should be
// loadable by pluginlib::ClassLoader must have these two lines
// compiled in its .cpp file, outside of any namespace scope.
#include <pluginlib/class_list_macros.h>
PLUGINLIB_DECLARE_CLASS( re2uta_ocuPlugins, ImgHandler, ImgHandlerPlugin, rviz::Panel )

