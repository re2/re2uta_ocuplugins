/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include <QHBoxLayout>
#include <QString>

#include <re2uta_ocuPlugins/SliderAndSpinBox.h>


SliderAndSpinBox::SliderAndSpinBox(QWidget *parent) : QWidget(parent)
{
    m_label = new QLabel("");

    m_slider  = new QSlider(Qt::Horizontal, this);
    m_spinBox = new QSpinBox(this);

    QHBoxLayout *hbox = new QHBoxLayout(this);
    hbox->addWidget(m_label);
    hbox->addWidget(m_slider);
    hbox->addWidget(m_spinBox);

    connect(m_slider,  SIGNAL(valueChanged(int)), this, SLOT(setSpinBox(int)));
    connect(m_spinBox, SIGNAL(valueChanged(int)), this, SLOT(setSlider( int)));

    setLayout(hbox);
}

void SliderAndSpinBox::setLabelTxt(std::string txt)
{
    QString* qtxt = new QString(txt.c_str());
    m_label->setText(*qtxt);
}

void SliderAndSpinBox::setRange(int min, int max)
{
    m_slider->setRange(min, max);
    m_spinBox->setRange(min, max);
}
    
void SliderAndSpinBox::setSlider(int val)
{
    if(getSliderVal() != val)
    {
        m_slider->setValue(val);
    }
}

void SliderAndSpinBox::setSpinBox(int val)
{
    if(getSpinBoxVal() != val)
    {
        m_spinBox->setValue(val);
    }
}

int SliderAndSpinBox::getSliderVal()
{
    return m_slider->value();
}

int SliderAndSpinBox::getSpinBoxVal()
{
    return m_spinBox->value();
}

void SliderAndSpinBox::setSliderVal(int val) 
{
    m_slider->setValue(val);
}
